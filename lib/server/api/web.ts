import { Service } from 'typedi';
import { Container } from 'typedi';
import { Router } from 'express';

import { AdminController } from '../../app/database/admin/admin.controller';
import { ActivityController } from '../../app/database/activity/activity.controller';
import { AuthController } from '../../app/database/auth/auth.controller';
import { ClientController } from '../../app/database/client/client.controller';
import { PointsController } from '../../app/database/points/points.controller';
import { RewardController } from '../../app/database/reward/reward.controller';
import { StatisticController } from '../../app/database/stadistic/stadistic.controller';
import { StoreController } from '../../app/database/store/store.controller';
import { TradeController } from '../../app/database/trade/trade.controller';
import verifyToken from 'app/middlewares/tokenValidator';


@Service()
export class ApiWeb {
  apiWebRouter: Router;

  constructor() {
    this.apiWebRouter = Router();
    this._setApiWebRoutes();
  };

  getApiWebRouter(): Router {
    return this.apiWebRouter;
  };

  private _setApiWebRoutes() {
    this._setAdminApiRoutes();
    this._setActivityApiRoutes();
    this._setClientApiRoutes();
    this._setPointsApiRoutes();
    this._setAuthApiRoutes();
    this._setStatisticApiRoutes();
    this._setTradeApiRoutes();
    this._setStoreApiRoutes();
    this._setRewardApiRoutes();
  };

  private _setAdminApiRoutes() {
    this.apiWebRouter.use('/admin', verifyToken);
    this.apiWebRouter.use('/admin', Container.get<AdminController>(AdminController).router);
  };

  private _setActivityApiRoutes() {
    this.apiWebRouter.use('/activity', verifyToken);
    this.apiWebRouter.use('/activity', Container.get<ActivityController>(ActivityController).router);
  };

  private _setAuthApiRoutes() {
    this.apiWebRouter.use('/auth', Container.get<AuthController>(AuthController).router);
  };

  private _setClientApiRoutes() {
    this.apiWebRouter.use('/client', verifyToken);
    this.apiWebRouter.use('/client', Container.get<ClientController>(ClientController).router);
  };

  private _setPointsApiRoutes() {
    this.apiWebRouter.use('/points', verifyToken);
    this.apiWebRouter.use('/points', Container.get<PointsController>(PointsController).router);
  };

  private _setRewardApiRoutes() {
    this.apiWebRouter.use('/reward', verifyToken);
    this.apiWebRouter.use('/reward', Container.get<RewardController>(RewardController).router);
  };

  private _setStatisticApiRoutes() {
    this.apiWebRouter.use('/stadistic', verifyToken);
    this.apiWebRouter.use('/stadistic', Container.get<StatisticController>(StatisticController).router);
  };

  private _setStoreApiRoutes() {
    this.apiWebRouter.use('/store', verifyToken);
    this.apiWebRouter.use('/store', Container.get<StoreController>(StoreController).router);
  };

  private _setTradeApiRoutes() {
    this.apiWebRouter.use('/trade', verifyToken);
    this.apiWebRouter.use('/trade', Container.get<TradeController>(TradeController).router);
  };

};