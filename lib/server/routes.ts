
import { Express } from 'express';
import * as express from 'express';
import { ApiWeb } from './api/web';

export function setRoutes(app: Express) {
  app.use('/app', express.static('client'));
  app.use('/app/*', express.static('client'));
  const apiWebRouter = new ApiWeb();
  app.use('/api/web', apiWebRouter.getApiWebRouter());

};

/*import { Request, Response } from "express";

export class Routes {
    public routes(app): void {

    }
};*/