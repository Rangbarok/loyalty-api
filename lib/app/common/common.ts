const _ = require('lodash');
// import * as crypto from 'crypto';

// const PRIVATE_HMAC_KEY = 'rg8gasaiksmshdfg4cw732';

/*export function isValidId(modelId): Boolean {
  if (typeof modelId === 'number' && modelId > 0) {
    return true;
  } else {
    return false;
  }
};*/

export function hasValue(value) {
  var result = !_.isNull(value) && !_.isUndefined(value);
  if (result) {
    if (typeof value === 'object' || typeof value === 'string') {
      if (_.isDate(value)) {
        return true;
      }
      return !_.isEmpty(value);
    } else if (typeof value === 'number') {
      return !_.isNaN(value);
    } else {
      return result;
    }
  } else {
    return false;
  }
};

export function isValidMongoId(id) {
  if (!id) {
		return false;
  }

	if (typeof id !== 'string') {
		id = id.toString();
  }

	if (id.match(/^[a-f\d]{24}$/i)) {
		return true;
	} else {
		return false;
  }
};

export function isNumber(value) {
  var intValue = _.parseInt(value);
  return !_.isNull(value) && !_.isUndefined(value) && !_.isNaN(intValue)
    && _.isNumber(intValue);
};

export function isString(value) {
  return typeof value === 'string';
};

export function isObject(obj) {
  return typeof obj === 'object';
};

export function isValidDateString(str) {
  if (typeof str !== 'string') {
    return false;
  } else {
    var date = new Date(str);
    return date.toString() !== 'Invalid Date';
  }
};

export function isValidAdmin(admin) {
  var isValidAdmin = false;
  if(this.hasValue(admin.username) && this.hasValue(admin.password) && this.hasValue(admin.email)) {
      isValidAdmin = true;
  }
  return isValidAdmin;
};

export function isValidClient(client) {
  var isValidClient = false;

  if(this.hasValue(client.username) && this.hasValue(client.password) && this.hasValue(client.email) &&
    this.hasValue(client.genre) && this.hasValue(client.username) && this.hasValue(client.birthdate)){
      isValidClient = true;
  }

    return isValidClient;
};

export function isValidTrade(trade) {
  var isValidTrade = false;

  if(this.hasValue(trade.storeId) && this.hasValue(trade.clientId) && this.hasValue(trade.rewardId)) {
    isValidTrade = true;
  }

  return isValidTrade;
};

export function isValidStore(store) {
  var isValidStore = false;
  if(this.hasValue(store.username) && this.hasValue(store.password) && this.hasValue(store.email) && this.hasValue(store.storeName) 
    && this.hasValue(store.username) && this.hasValue(store.location) && this.hasValue(store.location.latitude) && this.hasValue(store.location.longitude) 
    && this.hasValue(store.purchasePoints) && this.hasValue(store.purchaseFactor)) {
      isValidStore = true;
    }
  return isValidStore;
};

export function isValidReward(reward) {
  var isValidReward = false;
  if(this.hasValue(reward.storeId) && this.hasValue(reward.cost) && this.hasValue(reward.name) && this.hasValue(reward.description)
    && this.hasValue(reward.stock) && this.hasValue(reward.initDate) && this.hasValue(reward.endDate)) {
      isValidReward = true;
    }
  return isValidReward;
};

export function isValidPoints(points) {
  var isValidPoints = false;
  if(this.hasValue(points.clientId) && this.hasValue(points.storeId) && this.hasValue(points.points) && this.hasValue(points.deniedByStore)) {
      isValidPoints = true;
  }
  return isValidPoints;
};

export function isValidActivity(activity) {
  var isValidActivity = false;
  if(this.hasValue(activity.clientId) && this.hasValue(activity.storeId) && this.hasValue(activity.total)) {
    isValidActivity = true;
  }
  return isValidActivity;
};