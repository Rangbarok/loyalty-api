let jwt = require('jsonwebtoken');

let verifyToken = function(req,res,next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }

  token = token.replace('Bearer ', '');

  jwt.verify(token, 'SecretCode', function(err, tokenData) {
    if(err) {
      res.sendStatus(401);
    } else {
      next();
    }
  });

};

export default verifyToken;