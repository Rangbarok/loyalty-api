let jwt = require('jsonwebtoken');

export function getTokenData(req) {
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token.startsWith('Bearer ')) {
      token = token.slice(7, token.length);
    }
    token = token.replace('Bearer ', '');
    var dataJWT;
    jwt.verify(token, 'SecretCode', function(err, tokenData) {
        dataJWT = tokenData;
    });
    return dataJWT;
  };