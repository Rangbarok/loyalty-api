// 'Invalid Activity Id Error' Exception
export function InvalidActivityIdError() {
  this.message = 'Invalid Activity Id';
  this.name = 'InvalidActivityIdError';
}
InvalidActivityIdError.prototype = Object.create(Error.prototype);
InvalidActivityIdError.prototype.constructor = InvalidActivityIdError;

// 'Activity Input Validation Error' Exception
export function ActivityInputValidationError() {
  this.message = 'Activity Input Validation Error';
  this.name = 'ActivityInputValidationError';
}
ActivityInputValidationError.prototype = Object.create(Error.prototype);
ActivityInputValidationError.prototype.constructor = ActivityInputValidationError;

// 'Activity Not Found' Exception
export function ActivityNotFoundError() {
  this.message = 'Activity Not Found';
  this.name = 'ActivityNotFoundError';
}
ActivityNotFoundError.prototype = Object.create(Error.prototype);
ActivityNotFoundError.prototype.constructor = ActivityNotFoundError;