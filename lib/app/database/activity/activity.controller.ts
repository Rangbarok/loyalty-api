import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { ActivityService } from './activity.service';
import { Router } from 'express';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class ActivityController {
  public router: Router = Router({mergeParams: true});

  constructor( private activityService: ActivityService ) {
    autoBind(this);
    this.setPermissions();
    this.setRoutes();
  }

  setPermissions() { }

  setRoutes() {
    this.router.get('/activity', this.getAll);
    this.router.get('/activity/:id', this.getOne);
    this.router.get('/store/:storeId/activity', this.getAllByStore);
    this.router.get('/store/:storeId/activity/:id', this.getOneByStore);
    this.router.get('/client/:clientId/activity', this.getAllByClient);
    this.router.get('/client/:clientId/activity/:id', this.getOneByClient);
    this.router.post('/activity', this.create);
    this.router.delete('/activity/:id', this.delete);
  }

  /**
 * @api {get} /activity List all activities
 * @apiGroup Activity
 * @apiSuccess {Object[]} activity Activity's list
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Activity is active?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
  getAll(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    this.activityService.getAll()
      .then(function(activities){
        res.json(activities);
      });
  }

  /**
 * @api {get} /store/:id/activity List all activities for a store
 * @apiGroup Activity
 * @apiParam {ObjectId} id A store id
 * @apiSuccess {Object[]} activity Activity's list
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Activity is active?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 * @apiErrorExample {json} Activity not found
 *    HTTP/1.1 404 Activity Not Found
 */
  getAllByStore(req, res, next) {
    let storeId = req.params.storeId;
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store' || tokenData.userId != storeId){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.storeId)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    this.activityService.getAllByStore(storeId)
      .then(function(activities){
        res.json(activities);
      })
      .catch(InvalidStoreIdError => {
        next(new throwjs.NotFound(InvalidStoreIdError.message, 404));
      });
  }

  /**
 * @api {get} /client/:id/activity List all activities for a client
 * @apiGroup Activity
 * @apiParam {ObjectId} id A client id
 * @apiSuccess {Object[]} activity Activity's list
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Activity is active?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 * @apiErrorExample {json} Invalid Client Id Error
 *    HTTP/1.1 404 Invalid Client Id Error
 */
  getAllByClient(req, res, next) {
    let clientId = req.params.clientId;
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store' || tokenData.userId != clientId){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.clientId)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    this.activityService.getAllByClient(clientId)
      .then(function(activities){
        res.json(activities);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.NotFound(InvalidClientIdError.message, 404));
      });;
  }

  /**
 * @api {get} /activity/:id Get one activity
 * @apiGroup Activity
 * @apiParam {ObjectId} id Activity id
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Task is done?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 * @apiErrorExample {json} Activity Not Found Error
 *    HTTP/1.1 404 Not Found
 * @apiErrorExample {json} Invalid Activity Id Error
 *    HTTP/1.1 400 Invalid Activity Id Error
 */
  getOne(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store' && tokenData != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    let activityId = req.params.id;
    this.activityService.getOne(activityId)
      .then(function(activity){
        res.json(activity);
      })
      .catch(InvalidActivityIdError => {
        next(new throwjs.BadRequest(InvalidActivityIdError.message, 400));
      })
      .catch(ActivityNotFoundError => {
        next(new throwjs.NotFound(ActivityNotFoundError.message, 404));
      })
  }

  /**
 * @api {get} /client/:clientId/activity/:id Get one activity for a client
 * @apiGroup Activity
 * @apiParam {ObjectId} clientId A client id
 * @apiParam {ObjectId} id An activity id
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Task is done?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 * @apiErrorExample {json} Activity not found
 *    HTTP/1.1 404 Activity not found
 * @apiErrorExample {json} Invalid Activity Id Error
 *    HTTP/1.1 400 Invalid Activity Id Error
 */
  getOneByClient(req, res, next) {
    let clientId = req.params.clientId;
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'client' || tokenData.userId != clientId){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id) || !common.hasValue(req.params.clientId)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    let activityId = req.params.id;
    this.activityService.getOneByClient(clientId, activityId)
      .then(function(activity){
        res.json(activity);
      })
      .catch(InvalidActivityIdError => {
        next(new throwjs.BadRequest(InvalidActivityIdError.message, 400));
      })
      .catch(ActivityNotFoundError => {
        next(new throwjs.NotFound(ActivityNotFoundError.message, 400));
      });
  }

  /**
 * @api {get} /store/:storeId/activity/:id Get one activity for a store
 * @apiGroup Activity
 * @apiParam {ObjectId} storeId A store id
 * @apiParam {ObjectId} id An activity id
 * @apiSuccess {ObjectId} activity._id Activity id
 * @apiSuccess {ObjectId} activity.clientId Activity client
 * @apiSuccess {ObjectId} activity.storeId Activity store
 * @apiSuccess {ObjectId} activity.tradeId Activity trade (optional)
 * @apiSuccess {Number} activity.total Acivity total money
 * @apiSuccess {Number} activity.previousPoints Previous points of the client before of the operation
 * @apiSuccess {Number} activity.addedPoints Points added to the client for this operation
 * @apiSuccess {String} activity.description Activity details
 * @apiSuccess {Boolean} activity.active Task is done?
 * @apiSuccess {Date} activity.createdAt Creation's date
 * @apiSuccess {Date} activity.modifiedAt Register's date
 * @apiSuccess {Date} activity.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "tradeId": "5c3df6e57f668613e00672b0",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 * @apiErrorExample {json} Activity Not Found Error
 *    HTTP/1.1 404 Activity Not Found Error
 * @apiErrorExample {json} Invalid Activity Id Error
 *    HTTP/1.1 400 Invalid Activity Id Error
 * @apiErrorExample {json} Invalid Store Id Error
 *    HTTP/1.1 400 Invalid Store Id Error
 */
  getOneByStore(req, res, next) {
    var tokenData = getTokenData(req);
    let storeId = req.params.storeId;
    if (tokenData.role != 'admin' && tokenData.role != 'store' || tokenData.userId != storeId){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id) || !common.hasValue(req.params.clientId)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    let activityId = req.params.id;
    this.activityService.getOneByStore(storeId, activityId)
      .then(function(activity){
        res.json(activity);
      })
      .catch(InvalidActivityIdError => {
        next(new throwjs.BadRequest(InvalidActivityIdError.message, 400));
      })
      .catch(ActivityNotFoundError => {
        next(new throwjs.NotFound(ActivityNotFoundError.message, 404));
      })
      .catch(InvalidStoreIdError => {
        next(new throwjs.NotFound(InvalidStoreIdError.message, 400));
      });
  }

  /**
 * @api {post} /activity Register a new activity
 * @apiGroup Activity
 * @apiParamExample {json} Input
 *    {
 *      "clientId": "5c3df6e57f668613e00672c5",
 *      "storeId": "5c3df6e57f668613e00672b9",
 *      "total": 100,
 *      "previousPoints": 1000,
 *      "addedPoints": 100,
 *      "description": "Activity description",
 *      "active": false,
 *      "createdAt": 10/10/2019
 *    }
 * @apiSuccessExample {json} Created
 *    HTTP/1.1 201 Created
 * @apiErrorExample {json} Activity Input Validation Error
 *    HTTP/1.1 400 Activity Input Validation Error
 */
  create(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.body)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    this.activityService.createActivity(req.body)
      .then(function(admin) {
        res.sendStatus(HttpStatusCodes.CREATED).send(admin);
      })
      .catch(ActivityInputValidationError => {
        next(new throwjs(ActivityInputValidationError.message, 400));
      })
      .catch(next);
  }

  /**
 * @api {delete} /activity/:id Remove an activity
 * @apiGroup Activity
 * @apiParam {id} id Activity id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Activity Id Error
 *    HTTP/1.1 400 Invalid Activity Id Error
 * @apiErrorExample {json} Activity Not Found Error
 *    HTTP/1.1 404 Activity Not Found Error
 */
  delete(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    let activityId = req.params.id;

    this.activityService.removeActivity(activityId)
      .then(function() {
        res.sendStatus(HttpStatusCodes.OK);
      })
      .catch(InvalidActivityIdError => {
        next(new throwjs.BadRequest(InvalidActivityIdError.message, 400));
      })
      .catch(ActivityNotFoundError => {
        next(new throwjs.NotFound(ActivityNotFoundError.message, 404));
      })
      .catch(next);
  }
}