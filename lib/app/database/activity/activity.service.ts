import { ActivitySchema } from './activity.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common'
import { promises } from 'fs';
import { StoreService } from '../store/store.service';
import { PointsService } from '../points/points.service';

import { InvalidActivityIdError } from './activity.exceptions';
import { ActivityNotFoundError } from './activity.exceptions';
import { ActivityInputValidationError } from './activity.exceptions';
import { InvalidStoreIdError } from '../store/store.exceptions';
import { InvalidClientIdError} from '../client/client.exceptions';

const Activity = mongoose.model('Activity', ActivitySchema);

export class ActivityService {

    private storeService: StoreService;
    private pointsService: PointsService;

    async getAll() {
        return Activity.find({});
    }

    async getAllByStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        return Activity.find({storeId: storeId})
            .then(function(activities) {
                return activities;
            });
    }

    async getAllByClient(clientId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        return Activity.find({clientId: clientId})
            .then(function(activities) {
                return activities;
            });
    }

    async getOne(activityId) {
        if (!common.isValidMongoId(activityId)) {
            throw Promise.reject(new InvalidActivityIdError());
        }

        var query = Activity.findById(activityId);
        return query.exec()
          .then(function(activity){
            if(!common.hasValue(activity)){
              throw Promise.reject(new ActivityNotFoundError());
            }
            return activity;
          });
    }

    async getOneByClient(clientId, activityId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        if(!common.isValidMongoId(activityId)) {
            throw Promise.reject(new InvalidActivityIdError());
        }
        return Activity.find({_id: activityId, clientId: clientId})
            .then(function(activity) {
                if(!common.hasValue(activity)) {
                    throw Promise.reject(new ActivityNotFoundError());
                }
                return activity;
            });
    }

    async getOneByStore(storeId, activityId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        if(!common.isValidMongoId(activityId)) {
            throw Promise.reject(new InvalidActivityIdError());
        }

        return Activity.find({_id: activityId, storeId: storeId})
            .then(function(activity) {
                if(!common.hasValue(activity)) {
                    throw Promise.reject(new ActivityNotFoundError());
                }
                return activity;
            });
    }

    async createActivity(newActivity) {
        if(!common.isValidActivity(newActivity)){
            throw Promise.reject(new ActivityInputValidationError());
        }

        var purchaseFactor;
        var purchasePoints;
        this.storeService.getOne(newActivity.storeId)
            .then(function(store) {
                purchaseFactor = store.purchaseFactor;
                purchasePoints = store.purchasePoints;
            });

        var previousPoints;
        var pointsObject;
        this.pointsService.getOneByClientAndStore(newActivity.clientId, newActivity.storeId)
            .then(function(points) {
                previousPoints = points.points;
                pointsObject = points;
            });

        let pointsToAdd = (newActivity.total * purchaseFactor) + purchasePoints;

        newActivity.addedPoints = pointsToAdd;
        newActivity.previousPoints = previousPoints;

        pointsObject.points = pointsObject.points + pointsToAdd;
        pointsObject.modifiedAt = new Date();
        this.pointsService.updatePoints(pointsObject._id, pointsObject);

        Activity.create(newActivity, function (err, activity) {
            if (err) {
                return Promise.reject(new Error());
            }
            return activity;
          });
    }

    async removeActivity(activityId) {
        if(!common.isValidMongoId(activityId)) {
            throw Promise.reject(new InvalidActivityIdError());
        }

        return Activity.findById(activityId, function (err, activity) {
            if (err || !common.hasValue(activity)) {
                throw Promise.reject(new ActivityNotFoundError());
            }

            activity.active = false;
            activity.deletedAt = new Date();
            activity.save(err, deletedActivity => {
            if (err) {
                return Promise.reject(new Error(err));
            }

            return deletedActivity;
            });
          });
    }
};