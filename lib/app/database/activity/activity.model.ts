import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const ActivitySchema = new Schema({
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    tradeId: {
        type: mongoose.Schema.Types.ObjectId
    },
    total: {
        type: Number,
        required: true
    },
    previousPoints: {
        type: Number,
        required: true
    },
    addedPoints: {
        type: Number,
        required: true
    },
    description: {
        type: String
    },
    active: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});