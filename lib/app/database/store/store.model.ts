import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const StoreSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    storeName: {
        type: String,
        required: true
    },
    location: {
        latitude: {
            type: Number,
            required: true
          },
          longitude: {
            type: Number,
            required: true
          }
    },
    city: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
    purchaseFactor: {
        type: Number,
        required: true
    },
    purchasePoints: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});