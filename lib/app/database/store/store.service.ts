import { StoreSchema } from './store.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common';

import { TradeService } from '../trade/trade.service';
import { PointsService } from '../points/points.service';
import { ActivityService } from '../activity/activity.service';
import { RewardService } from '../reward/reward.service';

import {InvalidStoreIdError, StoreInputValidationError, StoreNotFoundError} from './store.exceptions';

const Store = mongoose.model('Store', StoreSchema);
export class StoreService {

    private tradeService: TradeService;
    private pointsService: PointsService;
    private activityService: ActivityService;
    private rewardService: RewardService;

    async getOne(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }
        return Store.find({_id: storeId}, function(store) {
            return store;
        });
    }

    async getAll() {
        return Store.find({}, function(stores) {
            return stores;
        })
    }

    async createStore(newStoreData) {
        if(!common.isValidStore(newStoreData)){
            throw Promise.reject(new StoreInputValidationError());
        }

        return Store.create(newStoreData, function (err, store) {
            if (err) {
                return Promise.reject(new Error());
            }
            return store;
          });
    }

    async deleteStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        this.tradeService.getAllByStore(storeId)
            .then(function(trades) {
                trades.forEach(trade => {
                    this.tradeService.deleteTrade(trade._id);
                });
            });

        this.pointsService.getAllByStore(storeId)
            .then(function(pointss) {
                pointss.forEach(points => {
                    this.pointsService.deletePoints(points._id);
                });
            });

        this.activityService.getAllByStore(storeId)
            .then(function(activities) {
                activities.forEach(activity => {
                    this.activityService.removeActivity(activity._id);
                });
            });

        this.rewardService.getAllByStore(storeId)
            .then(function(rewards) {
                rewards.forEach(reward => {
                    this.rewardService.deleteReward(reward._id);
                });
            })

        return Store.findById(storeId, function (err, store) {
            if (err) {
                throw Promise.reject(new StoreNotFoundError());
            }

            store.active = false;
            store.deletedAt = new Date();
            store.save(function (err, deletedStore) {
              if (err) {
                  return Promise.reject(new Error(err));
              }

              return deletedStore;
            });
          });
    }

    async updateStore(storeId, newStoreData) {
        if (!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        if (!common.isValidStore(newStoreData)){
            throw Promise.reject(new StoreInputValidationError());
        }

        Store.findByIdAndUpdate(storeId, newStoreData, function(err, store) {
            if(err) {
                throw Promise.reject(new Error(err));
            }
            return store;
        });
    }
};