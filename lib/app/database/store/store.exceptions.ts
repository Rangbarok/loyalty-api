export function LoginStoreError() {
  this.message = 'Login Store Error';
  this.name = 'LoginStoreError';
}
LoginStoreError.prototype = Object.create(Error.prototype);
LoginStoreError.prototype.constructor = LoginStoreError;

// 'Invalid Store Id Error' Exception
export function InvalidStoreIdError() {
  this.message = 'Invalid Store Id';
  this.name = 'InvalidStoreIdError';
}
InvalidStoreIdError.prototype = Object.create(Error.prototype);
InvalidStoreIdError.prototype.constructor = InvalidStoreIdError;

// 'Store Input Validation Error' Exception
export function StoreInputValidationError() {
  this.message = 'Store Input Validation Error';
  this.name = 'StoreInputValidationError';
}
StoreInputValidationError.prototype = Object.create(Error.prototype);
StoreInputValidationError.prototype.constructor = StoreInputValidationError;

// 'Store Not Found' Exception
export function StoreNotFoundError() {
  this.message = 'Store Not Found';
  this.name = 'StoreNotFoundError';
}
StoreNotFoundError.prototype = Object.create(Error.prototype);
StoreNotFoundError.prototype.constructor = StoreNotFoundError;