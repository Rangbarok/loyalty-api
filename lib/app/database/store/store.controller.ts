import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { StoreService } from './store.service';
import { Router } from 'express';
import { SSL_OP_NETSCAPE_CA_DN_BUG } from 'constants';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class StoreController {
    public router: Router = Router({mergeParams: true});

    constructor( private storeService: StoreService ) {
        autoBind(this);
        this.setPermissions();
        this.setRoutes();
    }

    setPermissions() { }

    setRoutes() {
        this.router.get('/store', this.getAll);
        this.router.get('/store/:id', this.getOne);
        this.router.post('/store', this.create);
        this.router.put('/store/:id', this.update);
        this.router.delete('/store/:id', this.remove);
    }

    /**
     * @api {get} /store List all stores
     * @apiGroup Store
     * @apiSuccess {Object[]} store Store's list
     * @apiSuccess {ObjectId} _id Store id
     * @apiSuccess {String} store.username Store username
     * @apiSuccess {String} store.password Store password
     * @apiSuccess {String} store.email Store email
     * @apiSuccess {String} store.storename Store name
     * @apiSuccess {Object} store.location Store location composed by lat and lng
     * @apiSuccess {String} store.city Store city
     * @apiSuccess {Number} store.purchasePoints Points added to client per purchase
     * @apiSuccess {Number} store.purchaseFactor Relation between total purchase and poitns added to client
     * @apiSucess {Boolean} store.active Store is active?
     * @apiSuccess {Date} store.createdAt Creation's date
     * @apiSuccess {Date} store.modifiedAt Register's date
     * @apiSuccess {Date} store.removedAt Delete's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [{
     *      "id": 1,
     *      "username": "store",
     *      "password": "pass",
     *      "email": "store@gmail.com",
     *      "storename": "store1",
     *      "location": {
     *          "lat": -48.0873,
     *          "lng": 0.68476
     *      },
     *      "city": "zaragoza",
     *      "purchasePoints": 50,
     *      "purchaseFactor": 0.5
     *      "active": true,
     *      "createdAt": 10/10/2019,
     *      "modifiedAt": 12/10/2019,
     *      "removedAt": 14/10/2019
     *    }]
     * @apiErrorExample {json} List error
     *    HTTP/1.1 500 Internal Server Error
     */
    getAll(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        this.storeService.getAll()
            .then(function(stores) {
                res.send(stores);
            })
            .catch(next);
    }

    /**
     * @api {get} /store/:id Find a store
     * @apiGroup Store
     * @apiSuccess {ObjectId} _id Store id
     * @apiSuccess {String} store.username Store username
     * @apiSuccess {String} store.password Store password
     * @apiSuccess {String} store.email Store email
     * @apiSuccess {String} store.storename Store name
     * @apiSuccess {Object} store.location Store location composed by lat and lng
     * @apiSuccess {String} store.city Store city
     * @apiSuccess {Number} store.purchasePoints Points added to client per purchase
     * @apiSuccess {Number} store.purchaseFactor Relation between total purchase and poitns added to client
     * @apiSucess {Boolean} store.active Store is active?
     * @apiSuccess {Date} store.createdAt Creation's date
     * @apiSuccess {Date} store.modifiedAt Register's date
     * @apiSuccess {Date} store.removedAt Delete's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "id": 1,
     *      "username": "store",
     *      "password": "pass",
     *      "email": "store@gmail.com",
     *      "storename": "store1",
     *      "location": {
     *          "lat": -48.0873,
     *          "lng": 0.68476
     *      },
     *      "city": "zaragoza",
     *      "purchasePoints": 50,
     *      "purchaseFactor": 0.5
     *      "active": true,
     *      "createdAt": 10/10/2019,
     *      "modifiedAt": 12/10/2019,
     *      "removedAt": 14/10/2019
     *    }
     * @apiErrorExample {json} Store Not Found Error
     *    HTTP/1.1 404 Store Not Found Error
     * @apiErrorExample {json} Invalid Store Id Error
     *    HTTP/1.1 400 Invalid Store Id Error
     */
    getOne(req, res, next) {
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        let storeId = req.params.id;

        this.storeService.getOne(storeId)
            .then(function(store){
                res.send(store);
            })
            .catch(StoreNotFoundError => {
                next(new throwjs.NotFound(StoreNotFoundError.message));
            })
            .catch(InvalidStoreIdError => {
                next(new throwjs.BadRequest(InvalidStoreIdError.message));
            })
            .catch(next);
    }

    /**
     * @api {post} /store Register a new store
     * @apiGroup Store
     * @apiParam {ObjectId} _id Store id
     * @apiParam {String} store.username Store username
     * @apiParam {String} store.password Store password
     * @apiParam {String} store.email Store email
     * @apiParam {String} store.storename Store name
     * @apiParam {Object} store.location Store location composed by lat and lng
     * @apiParam {String} store.city Store city
     * @apiParam {Number} store.purchasePoints Points added to client per purchase
     * @apiParam {Number} store.purchaseFactor Relation between total purchase and poitns added to client
     * @apiParam {Boolean} store.active Store is active?
     * @apiParamExample {json} Input
     *    {
     *      "username": "store",
     *      "password": "pass",
     *      "email": "store@gmail.com",
     *      "storename": "store1",
     *      "location": {
     *          "lat": -48.0873,
     *          "lng": 0.68476
     *      },
     *      "city": "zaragoza",
     *      "purchasePoints": 50,
     *      "purchaseFactor": 0.5,
     *      "active": true
     *    }
    * @apiSuccessExample {json} Created
    *    HTTP/1.1 204 Created
    * @apiErrorExample {json} Store not found
    *  HTTP/1.1 404 Not Found
   */
    create(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        this.storeService.createStore(req.body)
            .then(function(store) {
                res.sendStatus(HttpStatusCodes.CREATED);
            })
            .catch(StoreInputValidationError => {
                next(new throwjs.BadRequest(StoreInputValidationError.message));
            })
            .catch(next);
    }

    /**
   * @api {put} /admin/:id Update a store
   * @apiGroup Store
   * @apiParam {ObjectId} id Store id
   * @apiParam {String} username Store username
   * @apiParam {String} password Store password
   * @apiParam {String} email Store email
   * @apiParam {String} storename Store name
   * @apiParam {String} location Store location
   * @apiParam {String} city Store city
   * @apiParam {Number} purchasePoints Points added to client per purchase
   * @apiParam {Number} purchaseFactor Relation between total purchase and poitns added to client
   * @apiParamExample {json} Input
   *    {
   *      "id": 1,
   *      "username": "store",
   *      "password": "pass",
   *      "email": "store@gmail.com",
   *      "storename": "store1",
   *      "location": {
   *          "lat": -48.0873,
   *          "lng": 0.68476
   *      },
   *      "city": "zaragoza",
   *      "purchasePoints": 50,
   *      "purchaseFactor": 0.5
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 204 No Content
   * @apiErrorExample {json} Invalid Store Id Error
   *  HTTP/1.1 400 Invalid Store Id Error
   * @apiErrorExample {json} Store Not Found Error
   *  HTTP/1.1 404 Store Not Found Error
   */
    update(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        if(!common.hasValue(req.body)) {
            res.send(HttpStatusCodes.BAD_REQUEST);
        }

        let storeId = req.params.id;

        this.storeService.updateStore(storeId, req.body)
            .then(function(store) {
                res.sendStatus(HttpStatusCodes.NO_CONTENT);
            })
            .catch(InvalidStoreIdError => {
                next(new throwjs.BadRequest(InvalidStoreIdError.message));
            })
            .catch(StoreNotFoundError => {
                next(new throwjs.NotFound(StoreNotFoundError.message));
            })
            .catch(next);
    }

    /**
   * @api {delete} /store/:id Remove a store
   * @apiGroup Store
   * @apiParam {ObjectId} id Store id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 204 No Content
   * @apiErrorExample {json} Store not found
   *  HTTP/1.1 404 Not Found
   */
    remove(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        let storeId = req.params.id;

        this.storeService.deleteStore(storeId)
            .then(function(store){
                res.sendStatus(HttpStatusCodes.NO_CONTENT);
            })
            .catch(StoreNotFoundError => {
                next(new throwjs.NotFound(StoreNotFoundError.message));
            })
            .catch(next);
    }

}