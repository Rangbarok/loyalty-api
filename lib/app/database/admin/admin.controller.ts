import { Service, Container } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import { Router } from 'express';
import * as HttpStatusCodes from 'http-status-codes';
import { AdminService } from './admin.service';
import {getTokenData} from '../../middlewares/tokenData';
import { InvalidAdminIdError, AdminNotFoundError } from './admin.exceptions';
import { ActivityNotFoundError } from '../activity/activity.exceptions';

@Service()
export class AdminController {
  public router: Router = Router({mergeParams: true});
  public adminService: AdminService;

  constructor() {
    this.adminService = Container.get(AdminService);
    autoBind(this);
    this.setPermissions();
    this.setRoutes();
  }

  setPermissions() {}

  setRoutes() {
    this.router.get('/', this.getAll);
    this.router.get('/:id', this.getOne);
    this.router.post('/', this.create);
    this.router.put('/:id', this.update);
    this.router.patch('/:id', this.activate);
    this.router.delete('/:id', this.remove);
  }

  /**
 * @api {get} /admin List all admins
 * @apiGroup Admin
 * @apiSuccess {Object[]} admin Admin's list
 * @apiSuccess {ObjectId} admin._id Admin id
 * @apiSuccess {ObjectId} admin.username Admin username
 * @apiSuccess {ObjectId} admin.password Admin password
 * @apiSuccess {ObjectId} admin.email Admin email
 * @apiSuccess {Boolean} admin.active Admin is active? (Default FALSE)
 * @apiSuccess {Date} admin.createdAt Creation's date
 * @apiSuccess {Date} admin.modifiedAt Register's date
 * @apiSuccess {Date} admin.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "admin",
 *      "password": "pass",
 *      "email": "admin@apidoc.es",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
  getAll(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    this.adminService.getAll()
      .then(function(admins){
        res.send(admins);
      });
  }

  /**
 * @api {get} /admin/:id Get one admin
 * @apiGroup Admin
 * @apiParam id {ObjectId} Admin id
 * @apiSuccess {ObjectId} admin._id Admin id
 * @apiSuccess {String} admin.username Admin username
 * @apiSuccess {String} admin.password Admin password
 * @apiSuccess {String} admin.email Admin email
 * @apiSuccess {Boolean} admin.active Admin is active? (Default FALSE)
 * @apiSuccess {Date} admin.createdAt Creation's date
 * @apiSuccess {Date} admin.modifiedAt Register's date
 * @apiSuccess {Date} admin.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "admin",
 *      "password": "pass",
 *      "email": "admin@apidoc.es",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 * @apiErrorExample {json} Admin Not Found Error
 *    HTTP/1.1 404 Admin Not Found Error
 * @apiErrorExample {json} Invalid Admin Id Error
 *    HTTP/1.1 400 Invalid Admin id Error
 */
  getOne(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    let adminId = req.params.id;

    this.adminService.getOne(adminId)
      .then(function(admin){
        res.json(admin);
      })
      .catch(InvalidAdminIdError => {
        next(new throwjs.BadRequest(InvalidAdminIdError.message, 400));
      })
      .catch(AdminNotFoundError => {
        next(new throwjs.NotFound(AdminNotFoundError.message, 404));
      })
      .catch(next);
  }

  /**
 * @api {put} /admin/:id Update an admin
 * @apiGroup Admin
 * @apiParam {objectId} _id Admin id
 * @apiParamExample {json} Input
 *    {
 *      "username": "adminUpdate",
 *      "password": "passUpdate",
 *      "email": "adminUpdate@apidoc.es",
 *      "active": true
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Admin Input Validation Error
 *    HTTP/1.1 400 Admin Input Validation Error
 * @apiErrorExample {json} Invalid Admin Id Error
 *    HTTP/1.1 400 Invalid Admin Id Error
 * @apiErrorExample {json} Admin Not Found Error
 *    HTTP/1.1 404 Admin Not Found Error
 */
  update(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    let adminId = req.params.id;
    let newAdmin = {
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
      modifiedAt: new Date()
    };

    this.adminService.updateAdmin(adminId, newAdmin)
      .then(function(admin){
        if(common.hasValue(admin)) {
          res.sendStatus(HttpStatusCodes.NO_CONTENT);
        }
      })
      .catch(AdminInputValidationError => {
        next(new throwjs.BadRequest(AdminInputValidationError.message, 400));
      })
      .catch(InvalidAdminIdError => {
        next(new throwjs.BadRequest(InvalidAdminIdError.message, 400));
      })
      .catch(AdminNotFoundError => {
        next(new throwjs.NotFound(AdminNotFoundError.message, 404));
      })
      .catch(next);
  }

  /**
 * @api {delete} /admin/:id Remove an admin
 * @apiGroup Admin
 * @apiParam {objectId} id Admin id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Admin Id Error
 *    HTTP/1.1 400 Invalid Admin Id Error
 * @apiErrorExample {json} Admin Not Found Error
 *    HTTP/1.1 404 Admin Not Found Error
 */
  remove(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    let adminId = req.params.id;

    this.adminService.removeAdmin(adminId)
      .then(function(){
        res.sendStatus(HttpStatusCodes.NO_CONTENT);
      })
      .catch(InvalidAdminIdError => {
        next(new throwjs.BadRequest(InvalidAdminIdError.message, 400))
      })
      .catch(AdminNotFoundError => {
        next(new throwjs.NotFound(AdminNotFoundError.message, 404));
      })
      .catch(next);
  }

  /**
 * @api {post} /admin Register a new admin
 * @apiGroup Admin
 * @apiParamExample {json} Input
 *    {
 *      "username": "admin",
 *      "password": "pass",
 *      "email": admin@apidoc.es,
 *      "active": true,
 *      "createdAt": 10/10/2019
 *    }
 * @apiSuccessExample {json} Created
 *    HTTP/1.1 201 Created
 * @apiErrorExample {json} Activity Input Validation Error
 *    HTTP/1.1 400 Activity Input Validation Error
 */
  create(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.body)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }
    let newAdmin = {
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
      active: false,
      createdAt: new Date()
    };

    this.adminService.createAdmin(newAdmin)
      .then(function(admin) {
        res.sendStatus(HttpStatusCodes.NO_CONTENT).send(admin);
      })
      .catch(AdminInputValidationError => {
        next(new throwjs.BadRequest(AdminInputValidationError.message, 400));
      })
      .catch(next);
  }

  /**
 * @api {patch} /admin/:id Patch an admin to activate it
 * @apiGroup Admin
 * @apiParam {objectId} _id Admin id
 * @apiParamExample {json} Input
 *    {
 *      "active": true
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Admin Id Error
 *    HTTP/1.1 400 Invalid Admin Id Error
 */
  activate(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    let adminId = req.params.id;
    let active = req.body.active;

    this.adminService.activateAdmin(adminId, active)
      .then(function(admin){
        if(!common.hasValue(admin)) {
          next();
        }
        res.sendStatus(HttpStatusCodes.NO_CONTENT);
      })
      .catch(InvalidAdminIdError => {
        next(new throwjs.BadRequest(InvalidAdminIdError.message, 400));
      })
      .catch(next);
  }
}