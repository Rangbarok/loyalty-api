import { AdminSchema } from './admin.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common'
import { Service } from 'typedi';

import { InvalidAdminIdError } from './admin.exceptions';
import { AdminInputValidationError } from './admin.exceptions';
import { AdminNotFoundError } from './admin.exceptions';

const Admin = mongoose.model('Admin', AdminSchema);

@Service()
export class AdminService {

    async getAll() {
        var query = Admin.find({});
        return query.exec();
    }

    async getOne(adminId) {
        if (!common.isValidMongoId(adminId)) {
            throw Promise.reject(new InvalidAdminIdError());
        }

        var query = Admin.findById(adminId);
        return query.exec()
          .then(admin => {
            if(!common.hasValue(admin)){
              throw Promise.reject(new AdminNotFoundError());
            }
            return admin;
          });
    }

    async updateAdmin(adminId, newAdmin) {
        if (!common.isValidMongoId(adminId)) {
            throw Promise.reject(new InvalidAdminIdError());
        }

        if (!common.isValidAdmin(newAdmin)){
            throw Promise.reject(new AdminInputValidationError());
        }

        return Admin.findByIdAndUpdate(adminId, newAdmin, function(err, admin) {
            if(err || !common.hasValue(admin)) {
                throw Promise.reject(new AdminNotFoundError());
            }
            return admin;
        });
    }

    async removeAdmin(adminId) {
        if(!common.isValidMongoId(adminId)) {
            throw Promise.reject(new InvalidAdminIdError());
        }

        return Admin.findById(adminId, function (err, admin) {
            if (err) {
                throw Promise.reject(new AdminNotFoundError());
            }

            admin.active = false;
            admin.deletedAt = new Date();
            admin.save(function (err, deletedAdmin) {
              if (err) {
                  return Promise.reject(new Error(err));
              }

              return deletedAdmin;
            });
          });
    }

    async createAdmin(newAdmin) {
        if(!common.isValidAdmin(newAdmin)){
            throw Promise.reject(new AdminInputValidationError());
        }

        return Admin.create(newAdmin, function (err, admin) {
            if (err) {
                throw Promise.reject(new Error());
            }
            return admin;
          });
    }

    async activateAdmin(adminId, active) {
        if(!common.isValidMongoId(adminId)) {
            throw Promise.reject(new InvalidAdminIdError());
        }

        return Admin.findOne({_id: adminId}, function (err, admin) {
            admin.active = active;
            return admin.save(function (err) {
                if(err) {
                    throw Promise.reject(new Error(err));
                }
                return admin;
            });
        });
    }
};