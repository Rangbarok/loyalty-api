import { Container } from 'typedi';
import { AdminService } from './admin.service';
import { flushDb } from '../../testUtils/utils';
import { mongoose } from 'mongoose';
import { insertAdmin, getAdminId } from 'app/testUtils/admin.utils';

describe('Admin module', () => {
  const adminService = new AdminService();
  var adminIdInDB;

  beforeAll(() => {
    flushDb();
    insertAdmin();
    adminIdInDB = getAdminId();
  })

  describe('#getAll', () => {

    it('should returns all the admins', () => {
      adminService.getAll()
        .then(function(admins){
          expect(admins).not.toBeNull();
        });
    });
  });

  describe('#getOne', () => {

    it('should subtract properly', () => {
      adminService.getOne(adminIdInDB)
        .then(function(admin){
          expect(admin).not.toBeNull();
        });
    });

    it('should throw a InvalidAdminIdError', () => {
      return expect(adminService.getOne(1)).rejects.toThrow('InvalidAdminIdError');
    });
  });

  describe('#updateAdmin', () => {

    it('should update properly', () => {
      let adminObj = {
        username: 'admin2',
        password: 'pass',
        email: 'admin@admin.com'
      };
      adminService.updateAdmin(adminIdInDB, adminObj)
        .then(function(admin){
          var adminName = admin.username;
          expect(adminName).toBe('admin2');
        });
    });

    it('should throw a AdminNotFoundError', () => {
        const adminObj = {};
        return expect(adminService.updateAdmin(mongoose.Types.ObjectId(),adminObj)).rejects.toThrow('AdminNotFoundError');
    });
  });

  describe('#removeAdmin', () => {

    it('should remove the admin', () => {
      adminService.removeAdmin(adminIdInDB)
        .then(function(admin){
          expect(admin).not.toBeNull();
        });
    });

    it('should throw a AdminNotFoundError', () => {
      return expect(adminService.removeAdmin(adminIdInDB)).rejects.toThrow('AdminNotFoundError');
    });
  });

  describe('#activateAdmin', () => {

    it('should activate the admin', () => {
      adminService.removeAdmin(adminIdInDB)
        .then(function(result){
          expect(result).not.toBeNull();
        });
    });
  });

  describe('#createAdmin', () => {

    it('should create the admin', () => {
      adminService.removeAdmin(adminIdInDB)
        .then(function(admin){
          expect(admin).not.toBeNull();
        });
    });
  });

});
