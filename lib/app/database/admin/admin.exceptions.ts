export function LoginAdminError() {
  this.message = 'Login Admin Error';
  this.name = 'LoginAdminError';
}
LoginAdminError.prototype = Object.create(Error.prototype);
LoginAdminError.prototype.constructor = LoginAdminError;

// 'Invalid Admin Id Error' Exception
export function InvalidAdminIdError() {
  this.message = 'Invalid Admin Id';
  this.name = 'InvalidAdminIdError';
}
InvalidAdminIdError.prototype = Object.create(Error.prototype);
InvalidAdminIdError.prototype.constructor = InvalidAdminIdError;

// 'Admin Input Validation Error' Exception
export function AdminInputValidationError() {
  this.message = 'Admin Input Validation Error';
  this.name = 'AdminInputValidationError';
}
AdminInputValidationError.prototype = Object.create(Error.prototype);
AdminInputValidationError.prototype.constructor = AdminInputValidationError;

// 'Admin Not Found' Exception
export function AdminNotFoundError() {
  this.message = 'Admin Not Found';
  this.name = 'AdminNotFoundError';
}
AdminNotFoundError.prototype = Object.create(Error.prototype);
AdminNotFoundError.prototype.constructor = AdminNotFoundError;
