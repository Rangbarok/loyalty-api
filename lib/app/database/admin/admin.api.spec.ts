import * as request from 'supertest';
import * as HttpStatus from 'http-status-codes';
import app from "../../../app";
import { flushDb } from '../../testUtils/utils';
import { mongoose } from 'mongoose';
import { insertAdmin, getAdminId } from 'app/testUtils/admin.utils';

describe('Admin Controller API Module', () => {

  var mongoDB = 'mongodb://127.0.0.1/loyalty';
  mongoose.connect(mongoDB);
  mongoose.Promise = global.Promise;
  mongoose.set('useFindAndModify', false);
  var adminIdInDB;
  beforeAll(() => {
    flushDb();
    insertAdmin();
    adminIdInDB = getAdminId();
  });

  afterAll(() => {
    mongoose.connection.close();
  });

  describe('Get All Admins', () => {
    it('Should returns all admins', () => {
      const req = request(mongoose).get('/api/web/admin');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });

  describe('Get One Admin', () => {
    it('Should returns all admins', () => {
      const req = request(mongoose).get('/api/web/admin/' + this.adminIdInDB);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });

    it('Should reject invalid admin id error', () => {
      const req = request(mongoose).get('/api/web/admin/1');
      return req.then((res:any) => {
        expect(res.statusCode).toBe(HttpStatus.BAD_REQUEST);
      })
    });
  });


  describe('Create New Admin', () => {
    it('Should create the admin', () => {
      let adminObj = {
        username: 'admin',
        password: 'pass',
        email: 'admin@admin.com'
      };
      const req = request(mongoose).post('/api/web/admin', adminObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.CREATED);
      });
    });

    it('Should reject input validation error', () => {
      let adminObj = {
        test: 1
      };
      const req = request(mongoose).post('/api/web/admin', adminObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.BAD_REQUEST);
      });
    });
  });

  describe('Update An Admin', () => {
    it('Should update the admin', () => {
      let adminObj = {
        username: 'admin',
        password: 'pass',
        email: 'admin@admin.com'
      };
      const req = request(mongoose).put('/api/web/admin/' + this.adminIdInDB, adminObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });

    it('Should reject not found error', () => {
      let adminObj = {
        username: 'admin',
        password: 'pass',
        email: 'admin@admin.com'
      };
      const req = request(mongoose).put('/api/web/admin/' + mongoose.Types.ObjectId(), adminObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NOT_FOUND);
      });
    });
  });

  describe('Activate An Admin', () => {
    it('Should activate the admin', () => {
      const req = request(mongoose).patch('/api/web/admin/' + this.adminIdInDB, {active: true});
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NOT_FOUND);
      });
    });
  });

  describe('Remove An Admin', () => {
    it('Should remove the admin', () => {
      const req = request(mongoose).delete('/api/web/admin/' + this.adminIdInDB);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NO_CONTENT);
      });
    });
  })
})