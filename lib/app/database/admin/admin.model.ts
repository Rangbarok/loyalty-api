import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const AdminSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});