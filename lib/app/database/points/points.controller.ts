import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';
import { PointsService } from './points.service';
import { Router } from 'express';
import {getTokenData} from '../../middlewares/tokenData';
import { PointsInputValidationError, PointsNotFoundError, InvalidPointsIdError } from './points.exceptions';

@Service()
export class PointsController {
    public router: Router = Router({mergeParams: true});

    constructor( private pointsService: PointsService ) {
        autoBind(this);
        this.setPermissions();
        this.setRoutes();
    }

    setPermissions() {}

    setRoutes() {
        this.router.get('/points', this.getAll);
        this.router.get('/client/:id/points', this.getAllByClient);
        this.router.get('/store/:id/points', this.getAllByStore);
        this.router.get('/points/:id', this.getOne);
        this.router.post('/client/:id/points', this.create);
        this.router.put('/client/:id/points/:id', this.update);
        this.router.delete('/client/:id/points/:id', this.remove);
    }

    /**
 * @api {get} /points List all points
 * @apiGroup Points
 * @apiSuccess {Object[]} points Points' list
 * @apiSuccess {ObjectId} points._id Points' id
 * @apiSuccess {ObjectId} points.storeId Points' store
 * @apiSuccess {ObjectId} points.clientId Points' client
 * @apiSUccess {Number} points.points Points' quantity
 * @apiSuccess {Boolean} points.deniedByStore Client is blocket by the store?
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "points": 500,
 *      "deniedByStore": false,
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
    getAll(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
        res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        this.pointsService.getAll()
            .then(function(points) {
                res.send(points);
            });
    }

     /**
     * @api {get} /client/:id/points List all points for a client
     * @apiGroup Points
     * @apiParam {ObjectId} id Client id
     * @apiSuccess {Object[]} points Points' list
     * @apiSuccess {ObjectId} points._id Points' id
     * @apiSuccess {ObjectId} points.storeId Points' store
     * @apiSuccess {ObjectId} points.clientId Points' client
     * @apiSUccess {Number} points.points Points' quantity
     * @apiSuccess {Boolean} points.deniedByStore Client is blocket by the store?
     * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
     * @apiSuccess {Date} points.createdAt Creation's date
     * @apiSuccess {Date} points.modifiedAt Register's date
     * @apiSuccess {Date} points.removedAt Delete's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [{
     *      "_id": "5c3df6e57f668613e00672b4",
     *      "storeId": "5c3df6e57f668613e00672b5",
     *      "clientId": "5c3df6e57f668613e00672b6",
     *      "points": 500,
     *      "deniedByStore": false,
     *      "active": true,
     *      "createdAt": 10/10/2019,
     *      "modifiedAt": 12/10/2019,
     *      "removedAt": 14/10/2019
     *    }]
     */
    getAllByClient(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'client'){
        res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let clientId = req.params.id;
        this.pointsService.getAllByClient(clientId)
            .then(function(points){
                res.send(points);
            })
            .catch(InvalidClientIdError => {
                next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
            })
            .catch(next);
    }

     /**
 * @api {get} /store/:id/points List all points for a store
 * @apiGroup Points
 * @apiParam {ObjectId} id Store id
 * @apiSuccess {Object[]} points Points' list
 * @apiSuccess {ObjectId} points._id Points' id
 * @apiSuccess {ObjectId} points.storeId Points' store
 * @apiSuccess {ObjectId} points.clientId Points' client
 * @apiSUccess {Number} points.points Points' quantity
 * @apiSuccess {Boolean} points.deniedByStore Client is blocket by the store?
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "points": 500,
 *      "deniedByStore": false,
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
    getAllByStore(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
        res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let storeId = req.params.id;
        this.pointsService.getAllByStore(storeId)
            .then(function(points){
                res.send(points);
            })
            .catch(InvalidStoreIdError => {
                next(new throwjs.NotFound(InvalidStoreIdError.message, 404));
            })
            .catch(next);
    }

     /**
     * @api {get} /points/:id Get a points detail
     * @apiGroup Points
     * @apiParam {ObjectId} id Store id
     * @apiSuccess {ObjectId} points._id Points' id
     * @apiSuccess {ObjectId} points.storeId Points' store
     * @apiSuccess {ObjectId} points.clientId Points' client
     * @apiSUccess {Number} points.points Points' quantity
     * @apiSuccess {Boolean} points.deniedByStore Client is blocket by the store?
     * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
     * @apiSuccess {Date} points.createdAt Creation's date
     * @apiSuccess {Date} points.modifiedAt Register's date
     * @apiSuccess {Date} points.removedAt Delete's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "_id": "5c3df6e57f668613e00672b4",
     *      "storeId": "5c3df6e57f668613e00672b5",
     *      "clientId": "5c3df6e57f668613e00672b6",
     *      "points": 500,
     *      "deniedByStore": false,
     *      "active": true,
     *      "createdAt": 10/10/2019,
     *      "modifiedAt": 12/10/2019,
     *      "removedAt": 14/10/2019
     *    }
    */
    getOne(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'client' && tokenData != 'store'){
        res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        let pointsId = req.params.id;

        this.pointsService.getOne(pointsId)
            .then(function(points) {
                res.send(points);
            })
            .catch(InvalidPointsIdError => {
                next(new throwjs.BadRequest(InvalidPointsIdError.message, 400));
            })
            .catch(PointsNotFoundError => {
                next( new throwjs.NotFound(PointsNotFoundError.message, 404));
            })
            .catch(next);
    }

    /**
     * @api {get} /client/:clientId/store/:storeId/points/:id Get a points detail
     * @apiGroup Points
     * @apiParam {ObjectId} id Store id
     * @apiSuccess {ObjectId} points._id Points' id
     * @apiSuccess {ObjectId} points.storeId Points' store
     * @apiSuccess {ObjectId} points.clientId Points' client
     * @apiSUccess {Number} points.points Points' quantity
     * @apiSuccess {Boolean} points.deniedByStore Client is blocket by the store?
     * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
     * @apiSuccess {Date} points.createdAt Creation's date
     * @apiSuccess {Date} points.modifiedAt Register's date
     * @apiSuccess {Date} points.removedAt Delete's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "_id": "5c3df6e57f668613e00672b4",
     *      "storeId": "5c3df6e57f668613e00672b5",
     *      "clientId": "5c3df6e57f668613e00672b6",
     *      "points": 500,
     *      "deniedByStore": false,
     *      "active": true,
     *      "createdAt": 10/10/2019,
     *      "modifiedAt": 12/10/2019,
     *      "removedAt": 14/10/2019
     *    }
     * @apiErrorExample {json} Invalid Points Id Error
     *    HTTP/1.1 400 Invalid Points Id Error
    */
    getOneByClientAndStore(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'client' && tokenData.role != 'store'){
        res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id) || !common.hasValue(req.params.clientId)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
          }

          let clientId = req.params.clientId;
          let storeId = req.params.storeId;

          this.pointsService.getOneByClientAndStore(clientId, storeId)
            .then(function(activity){
              res.json(activity);
            })
            .catch(InvalidStoreIdError => {
              next(new throwjs.BadRequest(InvalidStoreIdError.message, 400));
            })
            .catch(InvalidClientIdError => {
              next(new throwjs.NotFound(InvalidClientIdError.message, 404));
            });
    }

    /**
     * @api {post} /points Register a new admin
     * @apiGroup Points
     * @apiParamExample {json} Input
     *    {
     *      "storeId": "5c3df6e57f668613e00672b5",
     *      "clientId": "5c3df6e57f668613e00672b6",
     *      "points": 500,
     *      "deniedByStore": false
     *    }
     * @apiSuccessExample {json} Created
     *    HTTP/1.1 201 Created
     * @apiErrorExample {json} Points Input Validation Error
     *    HTTP/1.1 400 Points Input Validation Error
    */
    create(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        this.pointsService.createPoints(req.body)
            .then(function(points) {
                res.sendStatus(HttpStatusCodes.CREATED);
            })
            .catch(PointsInputValidationError => {
                next(new throwjs.BadRequest(PointsInputValidationError.message, 404));
            })
            .catch(next);
    }

    /**
     * @api {put} /points/:id Update a points
     * @apiGroup Points
     * @apiParam {objectId} _id Points id
     * @apiParamExample {json} Input
     *    {
     *      "storeId": "5c3df6e57f668613e00672b5",
     *      "clientId": "5c3df6e57f668613e00672b6",
     *      "points": 500,
     *      "deniedByStore": false
     *    }
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 204 No Content
     * @apiErrorExample {json} Points Input Validation Error
     *    HTTP/1.1 400 Bad Request
     * @apiErrorExample {json} Invalid Points Id Error
     *    HTTP/1.1 400 Bad Request
     * @apiErrorExample {json} Points Not Found Error
     *    HTTP/1.1 404 Not Found
    */
    update(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id) || !common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let pointsId = req.params.id;
        this.pointsService.updatePoints(pointsId, req.body)
            .then(newPoints => {
                if(common.hasValue(newPoints)) {
                    res.sendStatus(HttpStatusCodes.OK);
                }
                res.sendStatus(HttpStatusCodes.BAD_REQUEST);
            })
            .catch(PointsNotFoundError => {
                next(new throwjs.NotFound(PointsNotFoundError.message, 404));
            })
            .catch(PointsInputValidationError => {
                next(new throwjs.BadRequest(PointsInputValidationError.message, 400));
            })
            .catch(next);
    }

    /**
     * @api {delete} /points/:id Remove a points
     * @apiGroup Points
     * @apiParam {objectId} id Points id
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 204 No Content
     * @apiErrorExample {json} Invalid Points Id Error
     *    HTTP/1.1 400 Invalid Points Id Error
     * @apiErrorExample {json} Points Not Found Error
     *    HTTP/1.1 404 Points Not Found Error
    */
    remove(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'client' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        let pointsId = req.params.id;

        this.pointsService.deletePoints(pointsId)
            .then(function(points){
                res.sendStatus(HttpStatusCodes.OK);
            })
            .catch(InvalidPointsIdError => {
                next(new throwjs.BadRequest(InvalidPointsIdError.message, 400));
            })
            .catch(PointsNotFoundError => {
                next(new throwjs.NotFound(PointsNotFoundError.message, 404));
            })
            .catch(next);
    }

}