import { PointsSchema } from './points.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { InvalidClientIdError } from '../client/client.exceptions';
import { InvalidStoreIdError } from '../store/store.exceptions';
import {InvalidPointsIdError, PointsInputValidationError, PointsNotFoundError} from './points.exceptions';

const Points = mongoose.model('Points', PointsSchema);
export class PointsService {

    async getAll() {
        return Points.find({}, function(points) {
            return points;
        })
    }

    async getOneByClientAndStore(clientId, storeId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        return Points.findOne({clientId: clientId, storeId: storeId}, function(points) {
            return points;
        })
    }

    async getAllByClient(clientId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }
        return Points.find({clientId: clientId}, function(points) {
            return points;
        });
    }

    async getAllByStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }
        return Points.find({storeId: storeId}, function(points) {
            return points;
        });
    }

    async getOne(pointsId) {
        if(!common.isValidMongoId(pointsId)) {
            throw Promise.reject(new InvalidPointsIdError());
        }
        return Points.findOne({_id: pointsId}, function(points) {
            return points;
        });
    }

    async createPoints(newPoints) {
        if(!common.isValidPoints(newPoints)){
            throw Promise.reject(new PointsInputValidationError());
        }

        return Points.create(newPoints, function (err, points) {
            if (err) {
                return Promise.reject(new Error());
            }
            return points;
          });
    }

    async updatePoints(pointsId, newPoints) {
        if (!common.isValidMongoId(pointsId)) {
            throw Promise.reject(new InvalidPointsIdError());
        }

        if (!common.isValidPoints(newPoints)){
            throw Promise.reject(new PointsInputValidationError());
        }

        Points.findByIdAndUpdate(pointsId, newPoints, function(err, points) {
            if(err) {
                throw Promise.reject(new Error(err));
            }
            return points;
        });
    }

    async deletePoints(pointsId) {
        if(!common.isValidMongoId(pointsId)) {
            throw Promise.reject(new InvalidPointsIdError());
        }

        return Points.findById(pointsId, function (err, points) {

            if(err || !common.hasValue(points)) {
                throw Promise.reject(new PointsNotFoundError());
            }

            points.active = false;
            points.deletedAt = new Date();
            points.save(function (err, deletedPoints) {
              if (err) {
                  return Promise.reject(new Error(err));
              }

              return deletedPoints;
            });
          });
    }

};