import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const PointsSchema = new Schema({
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        index: true
    },
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        index: true
    },
    points: {
        type: Number,
        required: true
    },
    deniedByStore: {
        type: Boolean,
        required: true,
        default: false
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});

PointsSchema.index({clientId: 1, storeId: 1});