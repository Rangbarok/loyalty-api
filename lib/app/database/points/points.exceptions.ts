// 'Invalid Points Id Error' Exception
export function InvalidPointsIdError() {
  this.message = 'Invalid Points Id';
  this.name = 'InvalidPointsIdError';
}
InvalidPointsIdError.prototype = Object.create(Error.prototype);
InvalidPointsIdError.prototype.constructor = InvalidPointsIdError;

// 'Points Input Validation Error' Exception
export function PointsInputValidationError() {
  this.message = 'Points Input Validation Error';
  this.name = 'PointsInputValidationError';
}
PointsInputValidationError.prototype = Object.create(Error.prototype);
PointsInputValidationError.prototype.constructor = PointsInputValidationError;

// 'Points Not Found' Exception
export function PointsNotFoundError() {
  this.message = 'Points Not Found';
  this.name = 'PointsNotFoundError';
}
PointsNotFoundError.prototype = Object.create(Error.prototype);
PointsNotFoundError.prototype.constructor = PointsNotFoundError;
