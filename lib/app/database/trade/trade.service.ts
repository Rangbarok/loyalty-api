import { TradeSchema } from './trade.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common';
import { RewardService } from '../reward/reward.service';
import { PointsService } from '../points/points.service';

import {InvalidTradeIdError, TradeInputValidationError} from './trade.exceptions';

const Trade = mongoose.model('Store', TradeSchema);
export class TradeService {

    private pointsService: PointsService;
    private rewardService: RewardService;

    async getOne(tradeId) {
        if(!common.isValidMongoId(tradeId)) {
            throw Promise.reject(new InvalidTradeIdError());
        }
        return Trade.findById(tradeId, function(err, trade) {
            if(err) {
                throw Promise.reject(err);
            }
            if(common.hasValue(trade)) {
                return trade;
            }
        });
    }

    async getAll() {
        return Trade.find({}, function(err, trades) {
            if(err) {
                throw Promise.reject(err);
            }
            return trades;
        });
    }

    async getAllByClient(clientId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidTradeIdError());
        }
        return Trade.find({clientId: clientId}, function(err, trades){
            if(err) {
                throw Promise.reject(err);
            }
            return trades;
        });
    }

    async getAllByStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidTradeIdError());
        }
        return Trade.find({storeId: storeId}, function(err, trades){
            if(err) {
                throw Promise.reject(err);
            }
            return trades;
        })
    }

    async createTrade(newTradeData) {
        if(!common.isValidTrade(newTradeData)) {
            throw Promise.reject(new TradeInputValidationError());
        }
        let newTrade = {
            storeId: newTradeData.storeId,
            clientId: newTradeData.clientId,
            rewardId: newTradeData.rewardId,
            createdAt: new Date(),
            active: true
        };

        var rewardCost;
        this.rewardService.getOneByStore(newTradeData.storeId, newTradeData.rewardId)
            .then(function(reward) {
                rewardCost = reward.cost;
                this.pointsService().getOneByClientAndStore(newTradeData.clientId, newTradeData.storeId)
                    .then(function(points) {
                        points.modifiedAt = new Date();
                        points.points = points.points - rewardCost;
                        this.pointsService.updatePoints(points._id, points);
                    });
            });


        return Trade.create(newTrade, function(err, trade) {
            if(err) {
                throw Promise.reject(err);
            }
            return trade;
        });
    }

    async deleteTrade(tradeId) {
        if(!common.isValidMongoId(tradeId)) {
            throw Promise.reject(new InvalidTradeIdError());
        }
        return Trade.find({_id: tradeId}, function(err, trade) {
            if (err) {
                throw Promise.reject(err);
            }
            return trade;
        })
        .then(trade => {
            if(trade.active === true) {
                trade.active = false;
                let actualDate = new Date();
                trade.removedAt = actualDate;
            }
            return trade.save(function(err) {
                if(err) {
                    throw Promise.reject(err);
                }
            });
        });
    }
};