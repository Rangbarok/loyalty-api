import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { TradeService } from './trade.service';
import { Router } from 'express';
import { SSL_OP_NETSCAPE_CA_DN_BUG } from 'constants';
import { runInNewContext } from 'vm';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class TradeController {
    public router: Router = Router({mergeParams: true});

    constructor( private tradeService: TradeService ) {
        autoBind(this);
        this.setPermissions();
        this.setRoutes();
    }

    setPermissions() { }

    setRoutes() {
        this.router.get('/trade', this.getAll);
        this.router.get('/trade/:id', this.getOne);
        this.router.get('/client/:clientId/trade', this.getAllByClient);
        this.router.get('/store/:storeId/trade', this.getAllByStore);
        this.router.post('/trade', this.create);
        this.router.delete('/trade/:id', this.remove);
    }

    /**
 * @api {get} /trade List all trades
 * @apiGroup Trade
 * @apiSuccess {Object[]} trades Trade's list
 * @apiSuccess {ObjectId} trade._id Trade's id
 * @apiSuccess {ObjectId} trade.storeId Trade's store
 * @apiSuccess {ObjectId} trade.clientId Trade's client
 * @apiSUccess {ObjectId} trade.rewardId Trade's quantity
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "rewardId": "5c3df6e57f668613e00672b8",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019
 *    }]
 */
    getAll(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        this.tradeService.getAll()
            .then(trades => {
                res.send(trades);
            })
    }

     /**
 * @api {get} /client/:id/trade List all trades
 * @apiGroup Trade
 * @apiParam {ObjectId} id Client id
 * @apiSuccess {Object[]} trades Trade's list
 * @apiSuccess {ObjectId} trade._id Trade's id
 * @apiSuccess {ObjectId} trade.storeId Trade's store
 * @apiSuccess {ObjectId} trade.clientId Trade's client
 * @apiSUccess {ObjectId} trade.rewardId Trade's quantity
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "rewardId": "5c3df6e57f668613e00672b8",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019
 *    }]
 */
    getAllByClient(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'client'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.clientId)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let clientId = req.params.clientId;
        this.tradeService.getAllByClient(clientId)
            .then(trades => {
                res.send(trades);
            })
            .catch(ClientTradeNotFoundError => {
                next(new throwjs.NotFound(ClientTradeNotFoundError.message));
            })
            .catch(next);
    }

     /**
 * @api {get} /store/:id/trade List all trades
 * @apiGroup Trade
 * @apiParam {ObjectId} id Store id
 * @apiSuccess {Object[]} trades Trade's list
 * @apiSuccess {ObjectId} trade._id Trade's id
 * @apiSuccess {ObjectId} trade.storeId Trade's store
 * @apiSuccess {ObjectId} trade.clientId Trade's client
 * @apiSUccess {ObjectId} trade.rewardId Trade's quantity
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "rewardId": "5c3df6e57f668613e00672b8",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019
 *    }]
 */
    getAllByStore(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.storeId)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let storeId = req.params.storeId;
        this.tradeService.getAllByClient(storeId)
            .then(trades => {
                res.send(trades);
            })
            .catch(InvalidTradeIdError => {
                next(new throwjs.BadRequest(InvalidTradeIdError.message));
            })
            .catch(StoreTradeNotFoundError => {
                next(new throwjs.NotFound(StoreTradeNotFoundError.message));
            })
            .catch(next);
    }

     /**
 * @api {get} /trade/:id List all trades
 * @apiGroup Trade
 * @apiParam {ObjectId} id Trade id
 * @apiSuccess {Object[]} trades Trade's list
 * @apiSuccess {ObjectId} trade._id Trade's id
 * @apiSuccess {ObjectId} trade.storeId Trade's store
 * @apiSuccess {ObjectId} trade.clientId Trade's client
 * @apiSUccess {ObjectId} trade.rewardId Trade's quantity
 * @apiSuccess {Boolean} points.active Client is active? (Default FALSE)
 * @apiSuccess {Date} points.createdAt Creation's date
 * @apiSuccess {Date} points.modifiedAt Register's date
 * @apiSuccess {Date} points.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "rewardId": "5c3df6e57f668613e00672b8",
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019
 *    }]
 */
    getOne(req, res, next) {
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let tradeId = req.params.id;
        this.tradeService.getOne(tradeId)
            .then(trade => {
                res.send(trade);
            })
            .catch(InvalidTradeIdError => {
                next(new throwjs.BadRequest(InvalidTradeIdError.message));
            })
            .catch(TradeNotFoundError => {
                next(new throwjs.NotFound(TradeNotFoundError.message));
            })
            .catch(next);
    }

    /**
 * @api {post} /trade Register a new trade
 * @apiGroup Trade
 * @apiParamExample {json} Input
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "clientId": "5c3df6e57f668613e00672b6",
 *      "rewardId": "5c3df6e57f668613e00672b8",
 *      "active": true
 *    }
 * @apiSuccessExample {json} Created
 *    HTTP/1.1 201 CREATED
 * @apiErrorExample {json} Trade Input Validation Error
 *    HTTP/1.1 400 Trade Input Validation Error
 */
    create(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        this.tradeService.createTrade(req.body)
            .then(trade => {
                res.sendStatus(HttpStatusCodes.CREATED);
            })
            .catch(TradeInputValidationError => {
                next(new throwjs.BadRequest(TradeInputValidationError.message));
            })
            .catch(next);
    }

    /**
 * @api {delete} /trade/:id Remove a trade
 * @apiGroup Trade
 * @apiParam {ObjectId} id Trade id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Trade Id Error
 *    HTTP/1.1 400 Invalid Trade Id Error
 * @apiErrorExample {json} Trade Not Found Error
 *    HTTP/1.1 404 Trde Not Found Error
 */
    remove(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let tradeId = req.params.id;
        this.tradeService.deleteTrade(tradeId)
            .then(trade => {
                res.sendStatus(HttpStatusCodes.NO_CONTENT);
            })
            .catch(InvalidTradeIdError => {
                next(new throwjs.BadRequest(InvalidTradeIdError.message), 400);
            })
            .catch(TradeNotFoundError => {
                next(new throwjs.BadRequest(TradeNotFoundError.message), 404);
            })
            .catch(next);
    }

}