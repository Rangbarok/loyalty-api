import * as request from 'supertest';
import * as HttpStatus from 'http-status-codes';
import app from "../../../app";
import { flushDb } from '../../testUtils/utils';
import { mongoose } from 'mongoose';
import { insertTrade, getTradeId, getClientId, getStoreId, getRewardId } from 'app/testUtils/trade.utils';

describe('Trade Controller API Module', () => {

  var mongoDB = 'mongodb://127.0.0.1/loyalty';
  mongoose.connect(mongoDB);
  mongoose.Promise = global.Promise;
  mongoose.set('useFindAndModify', false);
  var tradeIdInDb;
  var clientIdInDb;
  var storeIdInDb;
  var rewardIdInDb;

  beforeAll(() => {
    flushDb();
    insertTrade();
    tradeIdInDb = getTradeId();
    clientIdInDb = getClientId();
    storeIdInDb = getStoreId();
    rewardIdInDb = getRewardId();
  });

  afterAll(() => {
    mongoose.connection.close();
  });

  describe('Get All Trades', () => {
    it('Should returns all trades', () => {
      const req = request(mongoose).get('/api/web/trade');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });

  describe('Get One Trade', () => {
    it('Should return a trade', () => {
      const req = request(mongoose).get('/api/web/trade/' + tradeIdInDb);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });

    it('Should reject invalid trade id error', () => {
      const req = request(mongoose).get('/api/web/trade/1');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });


  describe('Get All Rewards By Client', () => {
    it('Should return all rewards of a client', () => {
      const req = request(mongoose).get('/api/web/client/' + clientIdInDb + '/trade');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });

  describe('Get All Rewards By Store', () => {
    it('Should return all rewards of a store', () => {
      const req = request(mongoose).get('/api/web/store/' + storeIdInDb + '/trade');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });

  describe('Create A Trade', () => {
    it('Should create the trade', () => {
      let tradeObj = {
        clientId: mongoose.Types.ObjectId(),
        storeId: mongoose.Types.ObjectId(),
        tradeId: mongoose.Types.ObjectId()
      };
      const req = request(mongoose).post('/api/web/trade', tradeObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.CREATED);
      });
    });

    it('Should reject input validation error', () => {
      let tradeObj = {
        test: 1
      };
      const req = request(mongoose).post('/api/web/trade');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.BAD_REQUEST);
      });
    });
  });

  describe('Remove A Trade', () => {
    it('Should remove the trade', () => {
      const req = request(mongoose).remove('/api/web/trade/' + tradeIdInDb);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NO_CONTENT);
      });
    });

    it('Should reject not found error', () => {
      const req = request(mongoose).remove('/api/web/trade/' + mongoose.Types.ObjectId());
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NOT_FOUND);
      });
    });
  })
})