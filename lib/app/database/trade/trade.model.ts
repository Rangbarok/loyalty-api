import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TradeSchema = new Schema({
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        index: true
    },
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        index: true
    },
    rewardId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    removedAt: {
        type: Date
    }
});