// 'Invalid Trade Id Error' Exception
export function InvalidTradeIdError() {
  this.message = 'Invalid Trade Id';
  this.name = 'InvalidTradeIdError';
}
InvalidTradeIdError.prototype = Object.create(Error.prototype);
InvalidTradeIdError.prototype.constructor = InvalidTradeIdError;

// 'Trade Input Validation Error' Exception
export function TradeInputValidationError() {
  this.message = 'Trade Input Validation Error';
  this.name = 'TradeInputValidationError';
}
TradeInputValidationError.prototype = Object.create(Error.prototype);
TradeInputValidationError.prototype.constructor = TradeInputValidationError;

// 'Trade Not Found' Exception
export function TradeNotFoundError() {
  this.message = 'Trade Not Found';
  this.name = 'TradeNotFoundError';
}
TradeNotFoundError.prototype = Object.create(Error.prototype);
TradeNotFoundError.prototype.constructor = TradeNotFoundError;