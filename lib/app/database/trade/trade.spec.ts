import { Container } from 'typedi';
import { TradeService } from './trade.service';
import { flushDb } from '../../testUtils/utils';
import * as mongoose from 'mongoose';
import * as HttpStatus from 'http-status-codes';
import { getTradeId, getClientId, getStoreId } from 'app/testUtils/trade.utils';

describe('Trade module', () => {
  const tradeService = new TradeService();

  var tradeIdInDb;
  var clientIdInDb;
  var storeIdInDb;

  beforeAll(() => {
    flushDb();
    tradeIdInDb = getTradeId();
    clientIdInDb = getClientId();
    storeIdInDb = getStoreId();
  })

  describe('#getAll', () => {

    it('should returns all trades', () => {
      tradeService.getAll()
        .then(function(trades){
          expect(trades).not.toBeNull();
        });
    });
  });

  describe('#getOne', () => {

    it('should return all a trade', () => {
      tradeService.getAllByStore(tradeIdInDb)
        .then(function(trades){
          expect(trades).not.toBeNull();
        });
    });

    it('should throw a InvalidTradeIdError', () => {
      expect(tradeService.getAllByStore(1)).rejects.toThrow('InvalidTradeIdError');
    });
  });

  describe('#getAllByClient', () => {

    it('should return all trades of a client', () => {
      tradeService.getAllByClient(clientIdInDb)
        .then(function(trades){
          expect(trades).not.toBeNull();
        });
    });
  });

  describe('#getAllByStore', () => {

    it('should return all trades of a store', () => {
      tradeService.getAllByStore(storeIdInDb)
        .then(function(trades){
          expect(trades).not.toBeNull();
        });
    });
  });

  describe('#deleteTrade', () => {

    it('should remove the trade', () => {
      tradeService.deleteTrade(tradeIdInDb)
        .then(function(trade){
          expect(trade).not.toBeNull();
        });
    });

    it('should reject not found error', () => {
      expect(tradeService.deleteTrade(tradeIdInDb)).rejects.toThrow('TradeNotFoundError');

    });
  });

  describe('#createTrade', () => {

    it('should create the trade', () => {
      tradeService.createTrade({})
        .then(function(result){
          expect(result).not.toBeNull();
        });
    });

    it('should reject input validation error', () => {
      let tradeObj = {
        test: 1
      };
      expect(tradeService.createTrade(tradeObj)).rejects.toThrow('InvalidTradeInputError');
    });
  });

});
