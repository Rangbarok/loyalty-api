import { Service, Container } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import { Router } from 'express';
import * as HttpStatusCodes from 'http-status-codes';

import { StatisticService } from './stadistic.service';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class StatisticController {
  public router: Router = Router({mergeParams: true});
  public stadisticService: StatisticService;

  constructor() {
    this.stadisticService = Container.get(StatisticService);
    autoBind(this);
    this.setPermissions();
    this.setRoutes();
  }

  setPermissions() { }

  setRoutes() {
    this.router.get('/genre', this.getGenreStatistic);
    this.router.get('/city', this.getCityStatistic);
  }

  /**
 * @api {get} /genre Obtain statistics of clients related with their genre
 * @apiGroup Statistic
 * @apiSuccess {Number} maleClient Indicates how many male clients are in the system
 * @apiSuccess {Number} femaleClient Indicates how many female clients are in the system
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "maleClients": 30,
 *      "femaleClients": 45
 *    }
 */
  private getGenreStatistic(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    this.stadisticService.getClientsGenreStatistic()
        .then(function(stadistic) {
            res.send(stadistic);
        })
  };

  /**
 * @api {get} /genre Obtain statistics of clients related with their genre
 * @apiGroup Statistic
 * @apiSuccess {Object[]} statistics Relation of stadistic object
 * @apiSuccess {Number} maleClient Indicates how many male clients are in the system
 * @apiSuccess {Number} femaleClient Indicates how many female clients are in the system
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [
 *      {
 *        "city": "Zaragoza",
 *        "totalCLients": 45
 *      },
 *      {
 *        "city": "Madrid",
 *        "totalCLients": 60
 *      }
 *    ]
 */
  private getCityStatistic(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    this.stadisticService.getClientsByCity()
        .then(function(stadistic) {
            res.send(stadistic);
        })
  }

}