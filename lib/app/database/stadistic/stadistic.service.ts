import * as mongoose from 'mongoose';
import * as common from '../../common/common';
import { ClientSchema } from '../client/client.model';
import { ClientService } from '../client/client.service';

const Client = mongoose.model('Reward', ClientSchema);
export class StatisticService {

    async getClientsGenreStatistic() {
        var femaleClients;
        Client.find({genre: 'female'}, function(femaleClients) {
            femaleClients = femaleClients;
        });

        var maleClients;
        Client.find({genre: 'male'}, function(maleClients) {
            maleClients = maleClients;
        });

        return {
            maleClients: maleClients.length,
            femaleClients: femaleClients.length
        };
    }

    async getClientsByCity() {
        var query = Client.aggregate({
            $group: {
                city: '$city',
                totalClients: { $sum: 1 }
            }
        });
        return query.exec();
    }

};