import * as mongoose from 'mongoose';
import * as common from '../../common/common'
import { Service } from 'typedi';
import { AdminSchema } from '../admin/admin.model';
import { ClientSchema } from '../client/client.model';
import { StoreSchema } from '../store/store.model';

import { LoginAdminError } from '../admin/admin.exceptions';
import { LoginClientError, ClientInputValidationError } from '../client/client.exceptions';
import { AdminNotFoundError } from '../admin/admin.exceptions';
import { ClientNotFoundError } from '../client/client.exceptions';

const Admin = mongoose.model('Admin', AdminSchema);
const Client = mongoose.model('Client', ClientSchema);
const Store = mongoose.model('Store', StoreSchema);

@Service()
export class AuthService {

    async createClient(newClient) {
        if(!common.isValidClient(newClient)){
            throw Promise.reject(new ClientInputValidationError());
        }

        return Client.create(newClient, function (err, client) {
            if (err) {
                return Promise.reject(new Error());
            }
            return client;
          });
    }

    async loginAdmin(loginData) {
        if (!common.hasValue(loginData) || !common.hasValue(loginData.username) || !common.hasValue(loginData.password)) {
            throw Promise.reject(new LoginAdminError());
        }
        let username = loginData.username;
        let password = loginData.password;
        var query = Admin.find({username: username, password: password});
        return query.exec()
            .then(admin => {
                if(!common.hasValue(admin)) {
                    throw Promise.reject(new LoginAdminError());
                }
                return admin;
            });
    }

    async loginClient(loginData) {
        if (!common.hasValue(loginData) || !common.hasValue(loginData.username) || !common.hasValue(loginData.password)) {
            throw Promise.reject(new LoginClientError());
        }
        let username = loginData.username;
        let password = loginData.password;
        var query = Client.find({username: username, password: password});
        return query.exec()
            .then(client => {
                if(!common.hasValue(client)) {
                    throw Promise.reject(new ClientNotFoundError());
                }
                return client;
            });
    }

    async loginStore(loginData) {
        if (!common.hasValue(loginData) || !common.hasValue(loginData.username) || !common.hasValue(loginData.password)) {
            throw Promise.reject(new LoginAdminError());
        }
        let username = loginData.username;
        let password = loginData.password;
        var query = Store.find({username: username, password: password});
        return query.exec()
            .then(store => {
                if(!common.hasValue(store)) {
                    throw Promise.reject(new AdminNotFoundError());
                }
                return store;
            });
    }
};