import { Service, Container } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import { Router } from 'express';
import * as HttpStatusCodes from 'http-status-codes';
import * as jwt from 'jsonwebtoken';

import { AuthService } from './auth.service';

@Service()
export class AuthController {
  public router: Router = Router({mergeParams: true});
  public authService: AuthService;

  constructor() {
    this.authService = Container.get(AuthService);
    autoBind(this);
    this.setPermissions();
    this.setRoutes();
  }

  setPermissions() { }

  setRoutes() {
    this.router.post('/loginAdmin', this.loginAdmin);
    this.router.post('/loginClient', this.loginClient);
    this.router.post('/loginStore', this.loginStore);
    this.router.post('/signUpClient', this.createClient);
  }

  /**
 * @api {post} /signUpClient Create a client without auth
 * @apiGroup Auth
 * @apiParamExample {json} Input
 *    {
 *      "username": "client",
 *      "password": "pass",
 *      "email": client@apidoc.es,
 *      "genre": "male",
 *      "birthDate": 10/10/1990,
 *      "city", "Zaragoza",
 *      "postalCode": 50015
 *    }
 * @apiSuccessExample {json} Created
 *    HTTP/1.1 201 Created
 * @apiErrorExample {json} Client Input Validation Error
 *    HTTP/1.1 400 Client Input Validation Error
 */
  createClient(req, res, next) {
    if(!common.hasValue(req.body)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    this.authService.createClient(req.body)
      .then(function(client) {
        res.sendStatus(HttpStatusCodes.CREATED);
      })
      .catch(ClientInputValidationError => {
        next(new throwjs.BadRequest(ClientInputValidationError.message, 400));
      })
      .catch(next);
  }

  /**
 * @api {post} /loginAdmin Login an admin in the system
 * @apiGroup Auth
 * @apiParamExample {json} Input
 *    {
 *      "username": "admin",
 *      "password": "pass"
 *    }
 * @apiSuccessExample {json} Login OK
 *    HTTP/1.1 200 OK
 *    {
 *      tokeN: eyJzdWIiOjE0LCJ1c2VybmFtZSI6InRlc3RVc2VyIiwiaWF0IjoxNTQwODI1MjIzLCJleHAiOjE1NDA4Njg0MjN9
 *    }
 * @apiErrorExample {json} Login Admin Error
 *    HTTP/1.1 401 Login Admin Error
 */
  loginAdmin(req, res, next) {
    this.authService.loginAdmin(req.body)
      .then(function(admin){
        if(!common.hasValue(admin)) {
          res.status(422);
          res.send('Invalid authentication');
        }

        var tokenData = {
          userId: admin._id,
          role: "admin"
        };

        var token = jwt.sign(tokenData, 'SecretCode', {
          expiresIn: 60*60*24
        });

        res.sendStatus(HttpStatusCodes.OK).send({
          token
        });
      })
      .catch(LoginAdminError => {
        next(new throwjs.BadRequest(LoginAdminError.message, 400));
      })
      .catch(next);
  }

  /**
 * @api {post} /loginAdmin Login a client in the system
 * @apiGroup Auth
 * @apiParamExample {json} Input
 *    {
 *      "username": "client",
 *      "password": "pass"
 *    }
 * @apiSuccessExample {json} Login OK
 *    HTTP/1.1 200 OK
 *    {
 *      tokeN: eyJzdWIiOjE0LCJ1c2VybmFtZSI6InRlc3RVc2VyIiwiaWF0IjoxNTQwODI1MjIzLCJleHAiOjE1NDA4Njg0MjN9
 *    }
 * @apiErrorExample {json} Login Client Error
 *    HTTP/1.1 401 Login Client Error
 */
  loginClient(req, res, next) {
    if(!common.hasValue(req.body)) {
        res.send(HttpStatusCodes.BAD_REQUEST);
      }
      this.authService.loginClient(req.body)
        .then(function(client){
          if(!common.hasValue(client)) {
            res.status(422);
            res.send('Invalid authentication');
          }

          var tokenData = {
            userId: client._id,
            role: "client"
          };

          var token = jwt.sign(tokenData, 'SecretCode', {
            expiresIn: 60*60*24
          });

          res.sendStatus(HttpStatusCodes.OK).send({
            token
          });
        })
        .catch(LoginClientError => {
          next(new throwjs.BadRequest(LoginClientError.message, 400));
        })
        .catch(ClientNotFoundError => {
          next(new throwjs.NotFound(ClientNotFoundError.message, 404));
        })
        .catch(next);
  }

  /**
 * @api {post} /loginAdmin Login a store in the system
 * @apiGroup Auth
 * @apiParamExample {json} Input
 *    {
 *      "username": "store",
 *      "password": "pass"
 *    }
 * @apiSuccessExample {json} Login OK
 *    HTTP/1.1 200 OK
 *    {
 *      tokeN: eyJzdWIiOjE0LCJ1c2VybmFtZSI6InRlc3RVc2VyIiwiaWF0IjoxNTQwODI1MjIzLCJleHAiOjE1NDA4Njg0MjN9
 *    }
 * @apiErrorExample {json} Login Store Error
 *    HTTP/1.1 401 Login Store Error
 */
  loginStore(req, res, next) {
    if(!common.hasValue(req.body)) {
        res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    this.authService.loginStore(req.body)
        .then(function(store) {
          if(!common.hasValue(store)) {
            res.status(422);
            res.send('Invalid authentication');
          }

          var tokenData = {
            userId: store._id,
            role: "store"
          };

          var token = jwt.sign(tokenData, 'SecretCode', {
            expiresIn: 60*60*24
          });

          res.send({
            token
          });
        })
        .catch(StoreNotFoundError => {
            next(new throwjs.NotFound(StoreNotFoundError.message));
        })
        .catch(LoginStoreError => {
            next(new throwjs.BadRequest(LoginStoreError.message));
        })
        .catch(next);
  }

}