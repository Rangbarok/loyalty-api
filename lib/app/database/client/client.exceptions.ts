export function LoginClientError() {
  this.message = 'Login Client Error';
  this.name = 'LoginClientError';
}
LoginClientError.prototype = Object.create(Error.prototype);
LoginClientError.prototype.constructor = LoginClientError;

// 'Invalid Client Id Error' Exception
export function InvalidClientIdError() {
  this.message = 'Invalid Client Id';
  this.name = 'InvalidClientIdError';
}
InvalidClientIdError.prototype = Object.create(Error.prototype);
InvalidClientIdError.prototype.constructor = InvalidClientIdError;

// 'Client Input Validation Error' Exception
export function ClientInputValidationError() {
  this.message = 'Client Input Validation Error';
  this.name = 'ClientInputValidationError';
}
ClientInputValidationError.prototype = Object.create(Error.prototype);
ClientInputValidationError.prototype.constructor = ClientInputValidationError;

// 'Client Not Found' Exception
export function ClientNotFoundError() {
  this.message = 'Client Not Found';
  this.name = 'ClientNotFoundError';
}
ClientNotFoundError.prototype = Object.create(Error.prototype);
ClientNotFoundError.prototype.constructor = ClientNotFoundError;