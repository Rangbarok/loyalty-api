import { ClientSchema } from './client.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common'
import { StoreService } from '../store/store.service';
import { TradeService } from '../trade/trade.service';
import { PointsService } from '../points/points.service';
import { ActivityService } from '../activity/activity.service';

import { InvalidClientIdError } from './client.exceptions';
import { ClientNotFoundError } from './client.exceptions';
import { InvalidStoreIdError } from '../store/store.exceptions';
import { ClientInputValidationError } from './client.exceptions';

const Client = mongoose.model('Client', ClientSchema);

export class ClientService {

    private tradeService: TradeService;
    private pointsService: PointsService;
    private activityService: ActivityService;

    async getOne(clientId) {
        if (!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        var query = Client.findById(clientId);
        return query.exec()
          .then(client => {
            if(!common.hasValue(client)){
              throw Promise.reject(new ClientNotFoundError());
            }
            return client;
          });
    }

    async getOneByStore(storeId, clientId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        var query = Client.find({
            _id: clientId,
            storeId: storeId
        });
        return query.exec()
            .then(client => {
                if(!common.hasValue(client)) {
                    throw Promise.reject(new ClientNotFoundError());
                }
                return client;
            });
    }

    async getAll() {
        var query = Client.find({});
        return query.exec();
    }

    async getAllByStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidStoreIdError());
        }

        return Client.find({storeId: storeId})
            .then(function(activities) {
                return activities;
            });
    }

    async updateClient(clientId, updatedClient) {
        if (!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        if (!common.isValidClient(updatedClient)){
            throw Promise.reject(new ClientInputValidationError());
        }

        Client.findByIdAndUpdate(clientId, updatedClient, function(err, client) {
            if(err) {
                throw Promise.reject(new Error(err));
            }
            return client;
        });
    }

    async patchClient(clientId, extraClientData) {
        if(!common.hasValue(extraClientData.city) || !common.hasValue(extraClientData.postalCode)) {
            throw Promise.reject(new ClientInputValidationError());
        }

        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        let client = Client.findById(clientId);

        if(!common.hasValue(client)) {
            throw Promise.reject(new ClientNotFoundError());
        }

        client.city = extraClientData.city;
        client.postalCode = extraClientData.postalCode;

        return client.save(function(err, client) {
            if(err) {
                return Promise.reject(new Error(err));
            }
            return client;
        });
    }

    async removeClient(clientId) {
        if(!common.isValidMongoId(clientId)) {
            throw Promise.reject(new InvalidClientIdError());
        }

        this.tradeService.getAllByClient(clientId)
            .then(function(trades) {
                trades.forEach(trade => {
                    this.tradeService.deleteTrade(trade._id);
                });
            });

        this.pointsService.getAllByClient(clientId)
            .then(function(pointss) {
                pointss.forEach(points => {
                    this.pointsService.deletePoints(points._id);
                });
            });

        this.activityService.getAllByClient(clientId)
            .then(function(activities) {
                activities.forEach(activity => {
                    this.activityService.removeActivity(activity._id);
                });
            });

        return Client.findById(clientId, function (err, client) {
            if (err) {
                throw Promise.reject(new ClientNotFoundError());
            }

            client.active = false;
            client.deletedAt = new Date();
            client.save(function (err, deletedAdmin) {
              if (err) {
                  return Promise.reject(new Error(err));
              }

              return deletedAdmin;
            });
          });
    }
};