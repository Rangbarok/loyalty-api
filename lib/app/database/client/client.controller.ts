import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { ClientService } from './client.service';
import { Router } from 'express';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class ClientController {
  public router: Router = Router({mergeParams: true});

  constructor( private clientService: ClientService ) {
    autoBind(this);
    this.setPermissions();
    this.setRoutes();
  }

  setPermissions() { }

  setRoutes() {
    this.router.get('/client', this.getAll);
    this.router.get('/client/:id', this.getOne);
    this.router.get('/store/:id/client', this.getAllByStore);
    this.router.get('/store/:id/client/:id', this.getOneByStore);
    this.router.put('/client/:id', this.update);
    this.router.patch('/client/:id', this.patch);
    this.router.delete('/client/:id', this.remove);
  }

  /**
 * @api {get} /client List all clients
 * @apiGroup Client
 * @apiSuccess {Object[]} client Client's list
 * @apiSuccess {ObjectId} client._id Client id
 * @apiSuccess {String} client.username Client's username
 * @apiSuccess {String} client.password Client's password
 * @apiSuccess {String} client.email Client's email
 * @apiSuccess {String} client.genre Client's genre
 * @apiSuccess {Date} client.birthdate Client's birthdate
 * @apiSuccess {String} client.city Client's city
 * @apiSuccess {Number} client.postalCode Client's postal code
 * @apiSuccess {Boolean} client.active Client is active? (Default FALSE)
 * @apiSuccess {Date} client.createdAt Creation's date
 * @apiSuccess {Date} client.modifiedAt Register's date
 * @apiSuccess {Date} client.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "client",
 *      "password": "pass",
 *      "email": "client@apidoc.es",
 *      "genre": "male",
 *      "birthdate": 10/10/1990,
 *      "city": "Zaragoza",
 *      "postalCode": 50015
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
  getAll(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    this.clientService.getAll()
      .then(function(clients) {
        res.send(clients);
      })
      .catch(next);
  }

  /**
 * @api {get} /store/:id/client List all clients
 * @apiGroup Client
 * @apiParam {ObjectId} id Store id
 * @apiSuccess {Object[]} client Client's list
 * @apiSuccess {ObjectId} client._id Client id
 * @apiSuccess {String} client.username Client's username
 * @apiSuccess {String} client.password Client's password
 * @apiSuccess {String} client.email Client's email
 * @apiSuccess {String} client.genre Client's genre
 * @apiSuccess {Date} client.birthdate Client's birthdate
 * @apiSuccess {String} client.city Client's city
 * @apiSuccess {Number} client.postalCode Client's postal code
 * @apiSuccess {Boolean} client.active Client is active? (Default FALSE)
 * @apiSuccess {Date} client.createdAt Creation's date
 * @apiSuccess {Date} client.modifiedAt Register's date
 * @apiSuccess {Date} client.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "client",
 *      "password": "pass",
 *      "email": "client@apidoc.es",
 *      "genre": "male",
 *      "birthdate": 10/10/1990,
 *      "city": "Zaragoza",
 *      "postalCode": 50015
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 * @apiErrorExample {json} Invalid Client Id Error
 *    HTTP/1.1 400 Invalid Client Id Error
 */
  getAllByStore(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    let storeId = req.params.id;

    this.clientService.getAllByStore(storeId)
      .then(function(clients) {
        res.send(clients);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(next);
  }

  /**
 * @api {get} /client/:id List all clients
 * @apiGroup Client
 * @apiParam {ObjectId} id Client id
 * @apiSuccess {Object[]} client Client's list
 * @apiSuccess {ObjectId} client._id Client id
 * @apiSuccess {String} client.username Client's username
 * @apiSuccess {String} client.password Client's password
 * @apiSuccess {String} client.email Client's email
 * @apiSuccess {String} client.genre Client's genre
 * @apiSuccess {Date} client.birthdate Client's birthdate
 * @apiSuccess {String} client.city Client's city
 * @apiSuccess {Number} client.postalCode Client's postal code
 * @apiSuccess {Boolean} client.active Client is active? (Default FALSE)
 * @apiSuccess {Date} client.createdAt Creation's date
 * @apiSuccess {Date} client.modifiedAt Register's date
 * @apiSuccess {Date} client.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "client",
 *      "password": "pass",
 *      "email": "client@apidoc.es",
 *      "genre": "male",
 *      "birthdate": 10/10/1990,
 *      "city": "Zaragoza",
 *      "postalCode": 50015
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 */
  getOne(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store' && tokenData.role != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    let clientId = req.params.id;

    this.clientService.getOne(clientId)
      .then(function(client) {
        res.send(client);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(ClientNotFoundError => {
        next(new throwjs.NotFound(ClientNotFoundError.message, 404));
      });
  }

  /**
 * @api {get} /store/:storeId/client/:id List all clients
 * @apiGroup Client
 * @apiParam {ObjectId} storeId Store id
 * @apiParam {ObjectId} id Client id
 * @apiSuccess {Object[]} client Client's list
 * @apiSuccess {ObjectId} client._id Client id
 * @apiSuccess {String} client.username Client's username
 * @apiSuccess {String} client.password Client's password
 * @apiSuccess {String} client.email Client's email
 * @apiSuccess {String} client.genre Client's genre
 * @apiSuccess {Date} client.birthdate Client's birthdate
 * @apiSuccess {String} client.city Client's city
 * @apiSuccess {Number} client.postalCode Client's postal code
 * @apiSuccess {Boolean} client.active Client is active? (Default FALSE)
 * @apiSuccess {Date} client.createdAt Creation's date
 * @apiSuccess {Date} client.modifiedAt Register's date
 * @apiSuccess {Date} client.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "username": "client",
 *      "password": "pass",
 *      "email": "client@apidoc.es",
 *      "genre": "male",
 *      "birthdate": 10/10/1990,
 *      "city": "Zaragoza",
 *      "postalCode": 50015
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 */
  getOneByStore(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'store' && tokenData.role != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.clientId) || !common.hasValue(req.params.storeId)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    let storeId = req.params.storeId;
    let clientId = req.params.clientId;

    this.clientService.getOneByStore(storeId, clientId)
      .then(function(client) {
        res.send(client);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(InvalidStoreIdError => {
        next(new throwjs.BadRequest(InvalidStoreIdError.message, 400));
      })
      .catch(ClientNotFoundError => {
        next(new throwjs.NotFound(ClientNotFoundError, 404))
      })
      .catch(next);
  }

  /**
 * @api {put} /client/:id Update an client
 * @apiGroup Client
 * @apiParam {objectId} _id Client id
 * @apiParamExample {json} Input
 *    {
 *      "username": "clientUpdate",
 *      "password": "pass",
 *      "email": "client@apidoc.es",
 *      "genre": "male",
 *      "birthdate": 10/10/1990,
 *      "city": "Zaragoza",
 *      "postalCode": 50015
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Client Input Validation Error
 *    HTTP/1.1 400 Client Input Validation Error
 * @apiErrorExample {json} Invalid Client Id Error
 *    HTTP/1.1 400 Invalid Client Id Error
 * @apiErrorExample {json} Client Not Found Error
 *    HTTP/1.1 404 Client Not Found Error
 */
  update(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    if(!common.hasValue(req.body)) {
      res.send(HttpStatusCodes.BAD_REQUEST);
    }

    let clientId = req.params.id;

    this.clientService.updateClient(clientId, req.body)
      .then(function(newClient) {
        res.send(newClient);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(ClientNotFoundError => {
        next(new throwjs.NotFound(ClientNotFoundError.message, 404));
      })
      .catch(next);
  }

  /**
 * @api {delete} /client/:id Remove an client
 * @apiGroup Client
 * @apiParam {objectId} id Client id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Client Id Error
 *    HTTP/1.1 400 Invalid Client Id Error
 * @apiErrorExample {json} Client Not Found Error
 *    HTTP/1.1 404 Client Not Found Error
 */
  remove(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    let clientId = req.params.id;

    this.clientService.removeClient(clientId)
      .then(function(client) {
        res.sendStatus(HttpStatusCodes.OK);
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(ClientNotFoundError => {
        next(new throwjs.NotFound(ClientNotFoundError.message, 404));
      })
      .catch(next);
  }

  /**
 * @api {patch} /client/:id Patch an client add more information
 * @apiGroup Client
 * @apiParam {objectId} _id cient id
 * @apiParamExample {json} Input
 *    {
 *      "city": Huesca,
 *      "postalCode": 50014
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Client Id Error
 *    HTTP/1.1 400 Invalid Client Id Error
 * @apiErrorExample {json} Client Not Found Error
 *    HTTP/1.1 400 Client Not Found Error
 * @apiErrorExample {json} Client Input Validation Error
 *    HTTP/1.1 400 Client Input Validation Error
 */
  patch(req, res, next) {
    var tokenData = getTokenData(req);
    if (tokenData.role != 'admin' && tokenData.role != 'client'){
      res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
    }
    if(!common.hasValue(req.params.id)) {
      res.sendStatus(HttpStatusCodes.BAD_REQUEST);
    }

    let clientId = req.params.id;

    if(!common.hasValue(req.body)) {
      res.send(HttpStatusCodes.BAD_REQUEST);
    }

    this.clientService.patchClient(clientId, req.body)
      .then(function(client) {
        res.send(client);
      })
      .catch(ClientInputValidationError => {
        next(new throwjs.BadRequest(ClientInputValidationError.message, 400));
      })
      .catch(InvalidClientIdError => {
        next(new throwjs.BadRequest(InvalidClientIdError.message, 400));
      })
      .catch(ClientNotFoundError => {
        next(new throwjs.NotFound(ClientNotFoundError.message, 404));
      })
      .catch(next);
  }

}