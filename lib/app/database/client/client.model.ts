import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const ClientSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    genre: {
        type: String,
        enum: ['male', 'female'],
        required: true
    },
    birthdate: {
        type: Date,
        required: true
    },
    city: {
        type: String
    },
    postalCode: {
        type: String
    },
    active: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});