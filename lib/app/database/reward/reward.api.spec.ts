import * as request from 'supertest';
import * as HttpStatus from 'http-status-codes';
import app from "../../../app";
import { flushDb } from '../../testUtils/utils';
import { mongoose } from 'mongoose';
import { insertReward, getRewardId, getStoreId } from 'app/testUtils/reward.utils';

describe('Reward Controller API Module', () => {

  var mongoDB = 'mongodb://127.0.0.1/loyalty';
  mongoose.connect(mongoDB);
  mongoose.Promise = global.Promise;
  mongoose.set('useFindAndModify', false);

  var rewardIdInDb;
  var storeIdInDb;

  beforeAll(() => {
    flushDb();
    insertReward();
    rewardIdInDb = getRewardId();
    storeIdInDb = getStoreId();
  });

  afterAll(() => {
    mongoose.connection.close();
  });

  describe('Get All Rewards', () => {
    it('Should returns all rewards', () => {
      const req = request(mongoose).get('/api/web/reward');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });
  });

  describe('Get All Rewards By Store', () => {
    it('Should returns all rewards', () => {
      const req = request(mongoose).get('/api/web/store/' + storeIdInDb + '/reward');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.OK);
      });
    });

    it('Should reject invalid reward id error', () => {
      const req = request(mongoose).get('/api/web/store/1/reward');
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.BAD_REQUEST);
      });
    });
  });


  describe('Get One Reward By Store', () => {
    it('Should return a reward from a store', () => {
      it('Should returns one reward', () => {
        const req = request(mongoose).get('/api/web/store/' + storeIdInDb + '/reward/' + rewardIdInDb);
        return req.then((res: any) => {
          expect(res.statusCode).toBe(HttpStatus.OK);
        });
      });
    });
  });

  describe('Create A Reward', () => {
    it('Should create the reward', () => {
      let rewardObj = {
        storeId: mongoose.Types.ObjectId(),
        cost: 10,
        name: 'reward'
      };
      const req = request(mongoose).post('/api/web/reward', rewardObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.CREATED);
      });
    });

    it('Should reject input validation error', () => {
      let rewardObj = {
        test: 1
      };
      const req = request(mongoose).post('/api/web/reward', rewardObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.BAD_REQUEST);
      });
    });
  });

  describe('Update A Reward', () => {
    it('Should update the reward', () => {
      let rewardObj = {
        storeId: mongoose.Types.ObjectId(),
        cost: 10,
        name: 'rewardTest'
      };
      const req = request(mongoose).put('/api/web/reward/' + rewardIdInDb, rewardObj);
      return req.then((res: any) => {
        expect(res.statusCode).toBe(HttpStatus.NO_CONTENT);
      });
    });
  });

  describe('Remove A Reward', () => {
    it('Should remove the reward', () => {
      const req = request(mongoose).delete('/api/web/store/' + storeIdInDb + '/reward/' + rewardIdInDb);
        return req.then((res: any) => {
          expect(res.statusCode).toBe(HttpStatus.OK);
        });
    });

    it('Should reject not found error', () => {
      const req = request(mongoose).delete('/api/web/store/' + mongoose.Types.ObjectId() + '/reward/' + rewardIdInDb);
        return req.then((res: any) => {
          expect(res.statusCode).toBe(HttpStatus.NOT_FOUND);
        });
    });
  })
})