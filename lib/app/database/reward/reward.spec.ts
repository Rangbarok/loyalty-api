import { Container } from 'typedi';
import { RewardService } from './reward.service';
import { flushDb } from '../../testUtils/utils'
import { getRewardId } from 'app/testUtils/reward.utils';
import { getStoreId } from 'app/testUtils/trade.utils';
import * as mongoose from 'mongoose';

describe('Reward module', () => {
  const rewardService = new RewardService();
  var rewardIdInDb;
  var storeIdInDb;

  beforeAll(() => {
    flushDb();
    rewardIdInDb = getRewardId();
    storeIdInDb = getStoreId();
  })

  describe('#getAll', () => {

    it('should returns all rewards', () => {
      rewardService.getAll()
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });
  });

  describe('#getAllByStore', () => {

    it('should return all reward of a store', () => {
      rewardService.getAllByStore(storeIdInDb)
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });

    it('should throw a InvalidRewardIdError', () => {
      return expect(rewardService.getAllByStore(1)).rejects.toThrow('InvalidRewardIdError');
    });
  });

  describe('#getOneByStore', () => {

    it('should return a reward of a store', () => {
      rewardService.getOneByStore(storeIdInDb, rewardIdInDb)
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });
  });

  describe('#createReward', () => {

    it('should remove the reward', () => {
      let rewardObj = {
        storeId: mongoose.Types.ObjectId(),
        cost: 10,
        name: 'reward'
      };
      rewardService.createReward(rewardObj)
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });

    it('should throw a InvalidRewardInputError', () => {
      let rewardObj = {
        test: 1
      };
      expect(rewardService.createReward(rewardObj)).rejects.toThrow('InvalidRewardInputError');
    });
  });

  describe('#updateReward', () => {

    it('should update the reward', () => {
      let rewardObj = {
        storeId: mongoose.Types.ObjectId(),
        cost: 100,
        name: 'reward2'
      };
      rewardService.updateReward(rewardIdInDb, rewardObj)
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });
  });

  describe('#deleteReward', () => {

    it('should remove the reward', () => {
      rewardService.deleteReward(rewardIdInDb)
        .then(function(reward){
          expect(reward).not.toBeNull();
        });
    });
  });

});
