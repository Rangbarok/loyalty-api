import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const RewardSchema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    cost: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    stock: {
        type: Number
    },
    initDate: {
        type: Date
    },
    endDate: {
        type: Date
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    modifiedAt: {
        type: Date
    },
    removedAt: {
        type: Date
    }
});