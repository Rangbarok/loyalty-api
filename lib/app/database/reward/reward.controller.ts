import { Service } from 'typedi';
import * as throwjs from 'throw.js';
import * as _ from 'lodash';
import * as autoBind from 'auto-bind';
import * as common from '../../common/common';
import * as HttpStatusCodes from 'http-status-codes';

import { RewardService } from './reward.service';
import { Router } from 'express';
import { InvalidRewardIdError } from './reward.exceptions';
import {getTokenData} from '../../middlewares/tokenData';

@Service()
export class RewardController {
    public router: Router = Router({mergeParams: true});

    constructor( private rewardService: RewardService ) {
        autoBind(this);
        this.setPermissions();
        this.setRoutes();
    }

    setPermissions() { }

    setRoutes() {
        this.router.get('/reward', this.getAll);
        this.router.get('/store/:id/reward', this.getAllByStore);
        this.router.get('/store/:id/reward:id', this.getOneByStore);
        this.router.post('/store/:id/reward', this.create);
        this.router.put('/store/:id/reward/:id', this.update);
        this.router.delete('/store/:id/reward/:id', this.remove);
    }

    /**
 * @api {get} /reward List all reward
 * @apiGroup Rewards
 * @apiSuccess {Object[]} rewards Reward list
 * @apiSuccess {String} reward._id Reward's id
 * @apiSuccess {String} reward.storeId Reward's store id
 * @apiSuccess {Number} reward.cost Reward's cost
 * @apiSuccess {String} reward.name Reward's name
 * @apiSUccess {String} reward.description Reward's description
 * @apiSuccess {Number} reward.stock Reward's stock
 * @apiSuccess {Date} reward.initDate Reward's init date
 * @apiSuccess {Date} reward.endDate Reward's end date
 * @apiSuccess {Boolean} reward.active Reward is active? (Default FALSE)
 * @apiSuccess {Date} reward.createdAt Creation's date
 * @apiSuccess {Date} reward.modifiedAt Register's date
 * @apiSuccess {Date} reward.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "cost": 500,
 *      "name": "TV",
 *      "description": "New TV",
 *      "stock": "New TV",
 *      "initDate": 10/10/2019,
 *      "endDate": 20/10/2019,
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
    getAll(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        this.rewardService.getAll()
            .then(function(rewards) {
                res.send(rewards);
            })
            .catch(next);
    }

        /**
 * @api {get} /store/:id/reward List all reward
 * @apiGroup Rewards
 * @apiParam {ObjectId} id Store id
 * @apiSuccess {Object[]} rewards Reward list
 * @apiSuccess {String} reward._id Reward's id
 * @apiSuccess {String} reward.storeId Reward's store id
 * @apiSuccess {Number} reward.cost Reward's cost
 * @apiSuccess {String} reward.name Reward's name
 * @apiSUccess {String} reward.description Reward's description
 * @apiSuccess {Number} reward.stock Reward's stock
 * @apiSuccess {Date} reward.initDate Reward's init date
 * @apiSuccess {Date} reward.endDate Reward's end date
 * @apiSuccess {Boolean} reward.active Reward is active? (Default FALSE)
 * @apiSuccess {Date} reward.createdAt Creation's date
 * @apiSuccess {Date} reward.modifiedAt Register's date
 * @apiSuccess {Date} reward.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "cost": 500,
 *      "name": "TV",
 *      "description": "New TV",
 *      "stock": "New TV",
 *      "initDate": 10/10/2019,
 *      "endDate": 20/10/2019,
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }]
 */
    getAllByStore(req, res, next) {
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let storeId = req.params.id;
        this.rewardService.getAllByStore(storeId)
            .then(rewards => {
                res.send(rewards);
            })
            .catch(StoreRewardNotFoundError => {
                next(new throwjs.NotFound(StoreRewardNotFoundError.message));
            })
            .catch(next);
    }

        /**
 * @api {get} /store/:storeId/reward/:id List all reward
 * @apiGroup Rewards
 * @apiParam {ObjectId} storeId Client id
 * @apiParam {ObjectId} id Reward id
 * @apiSuccess {String} reward._id Reward's id
 * @apiSuccess {String} reward.storeId Reward's store id
 * @apiSuccess {Number} reward.cost Reward's cost
 * @apiSuccess {String} reward.name Reward's name
 * @apiSUccess {String} reward.description Reward's description
 * @apiSuccess {Number} reward.stock Reward's stock
 * @apiSuccess {Date} reward.initDate Reward's init date
 * @apiSuccess {Date} reward.endDate Reward's end date
 * @apiSuccess {Boolean} reward.active Reward is active? (Default FALSE)
 * @apiSuccess {Date} reward.createdAt Creation's date
 * @apiSuccess {Date} reward.modifiedAt Register's date
 * @apiSuccess {Date} reward.removedAt Delete's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "_id": "5c3df6e57f668613e00672b4",
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "cost": 500,
 *      "name": "TV",
 *      "description": "New TV",
 *      "stock": "New TV",
 *      "initDate": 10/10/2019,
 *      "endDate": 20/10/2019,
 *      "active": true,
 *      "createdAt": 10/10/2019,
 *      "modifiedAt": 12/10/2019,
 *      "removedAt": 14/10/2019
 *    }
 * @apiErrorExample {json} Reward Not Found Error
 *    HTTP/1.1 404 Reward Not Found Error
 * @apiErrorExample {json} Invalid Reward Id Error
 *    HTTP/1.1 400 Invalid Reward Id Error
 */
    getOneByStore(req, res, next) {
        if(!common.hasValue(req.params.storeId) && !common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let storeId = req.params.storeId;
        let clientId = req.params.id;
        this.rewardService.getOneByStore(storeId, clientId)
            .then(rewards => {
                res.send(rewards);
            })
            .catch(RewardNotFoundError => {
                next(new throwjs.NotFound(RewardNotFoundError.message, 404));
            })
            .catch(InvalidRewardIdError => {
                next (new throwjs.BadRequest(InvalidRewardIdError.message, 400));
            })
            .catch(next);
    }

    /**
 * @api {post} /reward Register a new reward
 * @apiGroup Reward
 * @apiParamExample {json} Input
 *    {
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "cost": 500,
 *      "name": "TV",
 *      "description": "New TV",
 *      "stock": "New TV",
 *      "initDate": 10/10/2019,
 *      "endDate": 20/10/2019,
 *      "active": true
 *    }
 * @apiSuccessExample {json} No Content
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Reward Input Validation Error
 *    HTTP/1.1 400 Reward Input Validation Error
 */
    create(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' || tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        this.rewardService.createReward(req.body)
            .then(function() {
                res.sendStatus(HttpStatusCodes.NO_CONTENT);
            })
            .catch(RewardInputValidationError => {
                next(new throwjs.BadRequest(RewardInputValidationError.message));
            })
            .catch(next);
    }

    /**
 * @api {put} /reward/:id Update a reward
 * @apiGroup Reward
 * @apiParam {ObjectId} id Reward id
 * @apiParamExample {json} Input
 *    {
 *      "storeId": "5c3df6e57f668613e00672b5",
 *      "cost": 500,
 *      "name": "TV",
 *      "description": "New TV",
 *      "stock": "New TV",
 *      "initDate": 10/10/2019,
 *      "endDate": 20/10/2019,
 *      "active": true
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Reward Input Validation Error
 *    HTTP/1.1 400 Bad Request
 * @apiErrorExample {json} Invalid Reward Id Error
 *    HTTP/1.1 400 Bad Request
 * @apiErrorExample {json} Reward Not Found Error
 *    HTTP/1.1 404 Not Found
 */
    update(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id) || !common.hasValue(req.body)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }
        let rewardId = req.params.id;
        this.rewardService.updateReward(rewardId, req.body)
            .then(newReward => {
                if(common.hasValue(newReward)) {
                    res.sendStatus(HttpStatusCodes.NO_CONTENT);
                }
                res.sendStatus(HttpStatusCodes.BAD_REQUEST);
            })
            .catch(RewardInputValidationError => {
                next(new throwjs.BadRequest(RewardInputValidationError.message));
            })
            .catch(InvalidRewardIdError => {
                next(new throwjs.BadRequest(InvalidRewardIdError.message));
            })
            .catch(RewardNotFoundError => {
                next(new throwjs.NotFound(RewardNotFoundError.message));
            })
            .catch(next);
    }

    /**
 * @api {delete} /reward/:id Remove a reward
 * @apiGroup Reward
 * @apiParam {ObjectId} id Reward id
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Invalid Reward Id Error
 *    HTTP/1.1 400 Invalid Reward Id Error
 * @apiErrorExample {json} Reward Not Found Error
 *    HTTP/1.1 404 Reward Not Found Error
 */
    remove(req, res, next) {
        var tokenData = getTokenData(req);
        if (tokenData.role != 'admin' && tokenData.role != 'store'){
            res.sendStatus(HttpStatusCodes.UNAUTHORIZED);
        }
        if(!common.hasValue(req.params.id)) {
            res.sendStatus(HttpStatusCodes.BAD_REQUEST);
        }

        let rewardId = req.params.id;

        this.rewardService.deleteReward(rewardId)
            .then(function(store){
                res.sendStatus(HttpStatusCodes.OK);
            })
            .catch(InvalidRewardIdError => {
                next(new throwjs.BadRequest(InvalidRewardIdError.message, 400));
            })
            .catch(RewardNotFoundError => {
                next(new throwjs.NotFound(RewardNotFoundError.message, 404));
            })
            .catch(next);
    }

}