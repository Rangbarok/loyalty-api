import { RewardSchema } from './reward.model';
import * as mongoose from 'mongoose';
import * as common from '../../common/common';
import { StoreService } from '../store/store.service';

import {InvalidRewardIdError, RewardInputValidationError, RewardNotFoundError} from './reward.exceptions';

const Reward = mongoose.model('Reward', RewardSchema);
export class RewardService {

    private storeService: StoreService;

    async getAll() {
        return Reward.find({}, function(rewards) {
            return rewards;
        });
    }

    async getAllByStore(storeId) {
        if(!common.isValidMongoId(storeId)) {
            throw Promise.reject(new InvalidRewardIdError());
        }

        return Reward.find({storeId: storeId}, function(rewards) {
            return rewards;
        })
    }

    async getOneByStore(storeId, rewardId) {
        if(!common.isValidMongoId(storeId) || !common.isValidMongoId(rewardId)) {
            throw Promise.reject(new InvalidRewardIdError());
        }
        return Reward.findOne({_id: rewardId, storeId: storeId}, function(reward) {
            if(!common.hasValue(reward)) {
                throw Promise.reject(new RewardNotFoundError());
            }
            return reward;
        });
    }

    async createReward(newRewardData) {
        if(!common.isValidReward(newRewardData)){
            throw Promise.reject(new RewardInputValidationError());
        }

        return Reward.create(newRewardData, function (err, reward) {
            if (err) {
                return Promise.reject(new Error());
            }
            return reward;
          });
    }

    async deleteReward(rewardId) {
        if(!common.isValidMongoId(rewardId)) {
            throw Promise.reject(new InvalidRewardIdError());
        }

        return Reward.findById(rewardId, function (err, reward) {
            if (err) {
                throw Promise.reject(new RewardNotFoundError());
            }

            reward.active = false;
            reward.deletedAt = new Date();
            reward.save(function (err, deletedReward) {
              if (err) {
                  return Promise.reject(new Error(err));
              }

              return deletedReward;
            });
          });
    }

    async updateReward(rewardId, updateRewardData) {
        if (!common.isValidMongoId(rewardId)) {
            throw Promise.reject(new InvalidRewardIdError());
        }

        if (!common.isValidReward(updateRewardData)){
            throw Promise.reject(new RewardInputValidationError());
        }

        Reward.findByIdAndUpdate(rewardId, updateRewardData, function(err, reward) {
            if(err) {
                throw Promise.reject(new Error(err));
            }
            return reward;
        });
    }

};