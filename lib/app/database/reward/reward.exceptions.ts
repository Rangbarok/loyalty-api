// 'Invalid Reward Id Error' Exception
export function InvalidRewardIdError() {
  this.message = 'Invalid Reward Id';
  this.name = 'InvalidRewardIdError';
}
InvalidRewardIdError.prototype = Object.create(Error.prototype);
InvalidRewardIdError.prototype.constructor = InvalidRewardIdError;

// 'Reward Input Validation Error' Exception
export function RewardInputValidationError() {
  this.message = 'Reward Input Validation Error';
  this.name = 'RewardInputValidationError';
}
RewardInputValidationError.prototype = Object.create(Error.prototype);
RewardInputValidationError.prototype.constructor = RewardInputValidationError;

// 'Reward Not Found' Exception
export function RewardNotFoundError() {
  this.message = 'Reward Not Found';
  this.name = 'RewardNotFoundError';
}
RewardNotFoundError.prototype = Object.create(Error.prototype);
RewardNotFoundError.prototype.constructor = RewardNotFoundError;
