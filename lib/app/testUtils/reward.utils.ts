import { RewardSchema } from '../database/reward/reward.model';
import * as mongoose from 'mongoose';

const Reward = mongoose.model('Reward', RewardSchema);

let rewardId: String | undefined;
let storeId: string | undefined;

export function flush() {
  return Reward.collection.drop();
};

export function insertReward() {

  const rewardObj = {
    storeId: mongoose.Types.ObjectId(),
    cost: 200,
    name: 'reward name',
    description: 'reward description'
  };

  return Reward.create(rewardObj, function (err, reward) {
    rewardId = reward._id;
    storeId = reward.storeId;
    return reward;
  });
};

export function getRewardId(): String | undefined {
  return rewardId;
};

export function getStoreId(): string | undefined {
  return storeId;
};