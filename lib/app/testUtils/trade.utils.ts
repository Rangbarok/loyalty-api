import { TradeSchema } from '../database/trade/trade.model';
import * as mongoose from 'mongoose';

const Trade = mongoose.model('Trade', TradeSchema);

let tradeId: String | undefined;
let storeId: string | undefined;
let clientId: string | undefined;
let rewardId: string | undefined;

export function flush() {
  return Trade.collection.drop();
};

export function insertTrade() {

  const tradeObj = {
    storeId: mongoose.Types.ObjectId(),
    clientId: mongoose.Types.ObjectId(),
    rewardId: mongoose.Types.ObjectId()
  };

  return Trade.create(tradeObj, function (err, points) {
    tradeId = points._id;
    storeId = points.storeId;
    clientId = points.clientId;
    rewardId = points.rewardId;
    return points;
  });
};

export function getTradeId(): String | undefined {
  return tradeId;
};

export function getStoreId(): String | undefined {
  return storeId;
};

export function getClientId(): String | undefined {
  return clientId;
};

export function getRewardId(): String | undefined {
  return rewardId;
};