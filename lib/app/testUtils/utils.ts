import * as adminUtilsTest from './admin.utils';
import * as tradeUtilsTest from './trade.utils';
import * as rewardUtilsTest from './reward.utils';

export let adminUtils = adminUtilsTest;
export let tradeUtils = tradeUtilsTest;
export let rewardUtils = rewardUtilsTest;

export function flushDb() {
  return adminUtils.flush()
    .then(function(){
      tradeUtils.flush();
    })
    .then(function() {
      rewardUtilsTest.flush();
    });
};