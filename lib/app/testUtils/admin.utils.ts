import { AdminSchema } from '../database/admin/admin.model';
import * as mongoose from 'mongoose';

const Admin = mongoose.model('Admin', AdminSchema);

let adminId: String | undefined;

export function flush() {
  return Admin.collection.drop();
};

export function insertAdmin() {

  const adminObj = {
    username: 'admin',
    password: 'pass',
    email: 'admin@admin.com'
  };

  return Admin.create(adminObj, function (err, admin) {
    adminId = admin._id;
    return admin;
  });
};

export function getAdminId(): String | undefined {
  return adminId;
};