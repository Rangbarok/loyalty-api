import app from "./app";
import { setRoutes } from "./server/routes";
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';

var mongoDB = 'mongodb://127.0.0.1/loyalty';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

app.use(bodyParser.urlencoded({ extended: true }));
app.set('PORT', process.env.PORT || 3000);

setRoutes(app);
console.log('Routes assembled in app');

app.listen(app.get('PORT'), () => {
    console.log('Express server listening on port => ' + app.get('PORT'));
});